var elasticsearch = require("elasticsearch");
var client = new elasticsearch.Client({
    host: "localhost:9200",
    log: "trace",
    apiVersion: "7.2", // use the same version of your Elasticsearch instance
});

client.ping(
    {
        // ping usually has a 3000ms timeout
        requestTimeout: 5000,
    },
    function (error) {
        if (error) {
            console.trace("elasticsearch cluster is down!");
        } else {
            console.log("All is well");
        }
    }
);

async function createIndex(index) {
    try {
        const response = await client.indices.create({
            index: index.index,
            // type: '_doc',
            body: index.payload,
        });
        console.log(response);
    } catch (error) {
        console.trace(error.message);
    }
}

(async function _start() {
    // v3partnodegroups -----------------------------------
    const newIndex1 = {
        index: "v3partnodegroups",
        payload: {
            settings: {
                analysis: {
                    filter: {
                        russian_stop: { type: "stop", stopwords: "_russian_" },
                        russian_keywords: {
                            type: "keyword_marker",
                            keywords: ["примера"],
                        },
                        russian_stemmer: {
                            type: "stemmer",
                            language: "russian",
                        },
                        custom_stem: {
                            type: "stemmer_override",
                            rules_path: "analysis/rules.txt",
                        },
                    },
                    analyzer: {
                        rebuilt_russian: {
                            tokenizer: "standard",
                            filter: [
                                "lowercase",
                                "russian_stop",
                                "russian_keywords",
                                "russian_stemmer",
                                "custom_stem",
                            ],
                        },
                    },
                },
            },
            mappings: {
                properties: {
                    nameDisplay: {
                        type: "text",
                        fields: {
                            keyword: { type: "keyword", ignore_above: 256 },
                        },
                        analyzer: "rebuilt_russian",
                        boost: 0.1,
                    },
                    name: {
                        type: "text",
                        fields: {
                            keyword: { type: "keyword", ignore_above: 256 },
                        },
                        analyzer: "rebuilt_russian",
                        boost: 3.0,
                    },
                    positionalTags: {
                        type: "text",
                        fields: {
                            keyword: { type: "keyword", ignore_above: 256 },
                        },
                        analyzer: "rebuilt_russian",
                        boost: 2.0,
                    },

                    tags: {
                        type: "text",
                        fields: {
                            keyword: { type: "keyword", ignore_above: 256 },
                        },
                        analyzer: "rebuilt_russian",
                    },
                    units: {
                        type: "text",
                        fields: {
                            keyword: { type: "keyword", ignore_above: 256 },
                        },
                        analyzer: "rebuilt_russian",
                        boost: 0.5,
                    },
                },
            },
        },
    };
    // v3partnodes -----------------------------------
    const newIndex2 = {
        index: "v3partnodes",
        payload: {
            settings: {
                analysis: {
                    filter: {
                        russian_stop: { type: "stop", stopwords: "_russian_" },
                        russian_keywords: {
                            type: "keyword_marker",
                            keywords: ["примера"],
                        },
                        russian_stemmer: {
                            type: "stemmer",
                            language: "russian",
                        },
                        custom_stem: {
                            type: "stemmer_override",
                            rules_path: "analysis/rules.txt",
                        },
                    },
                    analyzer: {
                        rebuilt_russian: {
                            tokenizer: "standard",
                            filter: [
                                "lowercase",
                                "russian_stop",
                                "russian_keywords",
                                "russian_stemmer",
                                "custom_stem",
                            ],
                        },
                    },
                },
            },
            mappings: {
                properties: {
                    nameDisplay: {
                        type: "text",
                        fields: {
                            keyword: { type: "keyword", ignore_above: 256 },
                        },
                        analyzer: "rebuilt_russian",
                        boost: 0.1,
                    },
                    name: {
                        type: "text",
                        fields: {
                            keyword: { type: "keyword", ignore_above: 256 },
                        },
                        analyzer: "rebuilt_russian",
                        boost: 3.0,
                    },
                    positionalTags: {
                        type: "text",
                        fields: {
                            keyword: { type: "keyword", ignore_above: 256 },
                        },
                        analyzer: "rebuilt_russian",
                        boost: 2.0,
                    },

                    tags: {
                        type: "text",
                        fields: {
                            keyword: { type: "keyword", ignore_above: 256 },
                        },
                        analyzer: "rebuilt_russian",
                    },
                    units: {
                        type: "text",
                        fields: {
                            keyword: { type: "keyword", ignore_above: 256 },
                        },
                        analyzer: "rebuilt_russian",
                        boost: 0.5,
                    },
                    partnodegroup: {
                        type: "text",
                        fields: {
                            keyword: { type: "keyword", ignore_above: 256 },
                        },
                    },
                },
            },
        },
    };

    // v3genparts -----------------------------------
    const newIndex3 = {
        index: "v3genparts",
        payload: {
            settings: {
                analysis: {
                    filter: {
                        russian_stop: { type: "stop", stopwords: "_russian_" },
                        russian_keywords: {
                            type: "keyword_marker",
                            keywords: ["примера"],
                        },
                        russian_stemmer: {
                            type: "stemmer",
                            language: "russian",
                        },
                        custom_stem: {
                            type: "stemmer_override",
                            rules_path: "analysis/rules.txt",
                        },
                    },
                    analyzer: {
                        rebuilt_russian: {
                            tokenizer: "standard",
                            filter: [
                                "lowercase",
                                "russian_stop",
                                "russian_keywords",
                                "russian_stemmer",
                                "custom_stem",
                            ],
                        },
                    },
                },
            },
        },
    };
    await createIndex(newIndex1);
    await createIndex(newIndex2);

    await createIndex(newIndex3);
})();
