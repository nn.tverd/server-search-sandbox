var elasticsearch = require("elasticsearch");
var client = new elasticsearch.Client({
    host: "localhost:9200",
    log: "trace",
    apiVersion: "7.2", // use the same version of your Elasticsearch instance
});

client.ping(
    {
        // ping usually has a 3000ms timeout
        requestTimeout: 5000,
    },
    function (error) {
        if (error) {
            console.trace("elasticsearch cluster is down!");
        } else {
            console.log("All is well");
        }
    }
);

async function putMapping() {
    const newMapping = {
        index: "partans",
        // type: "_doc",
        payload: {
            // _doc: {
            //     mappings: {
            properties: {
                desc: {
                    type: "text",
                    fields: {
                        keyword: {
                            type: "keyword",
                            ignore_above: 256,
                        },
                    },
                },
                gn: {
                    type: "nested",
                    properties: {
                        gnId: {
                            type: "text",
                            fields: {
                                keyword: {
                                    type: "keyword",
                                    ignore_above: 256,
                                },
                            },
                        },
                        relScore: {
                            type: "long",
                        },
                    },
                },
                gp: {
                    type: "nested",
                    properties: {
                        gp: {
                            type: "text",
                            fields: {
                                keyword: {
                                    type: "keyword",
                                    ignore_above: 256,
                                },
                            },
                        },
                        relScore: {
                            type: "long",
                        },
                    },
                },
                imagesUrl: {
                    properties: {
                        iUrl: {
                            type: "text",
                            fields: {
                                keyword: {
                                    type: "keyword",
                                    ignore_above: 256,
                                },
                            },
                        },
                        relScore: {
                            type: "long",
                        },
                    },
                },
                md: {
                    type: "nested",
                    properties: {
                        mdId: {
                            type: "text",
                            fields: {
                                keyword: {
                                    type: "keyword",
                                    ignore_above: 256,
                                },
                            },
                        },
                        relScore: {
                            type: "long",
                        },
                    },
                },
                metaDesc: {
                    type: "text",
                    fields: {
                        keyword: {
                            type: "keyword",
                            ignore_above: 256,
                        },
                    },
                },
                mf: {
                    type: "nested",
                    properties: {
                        mfId: {
                            type: "text",
                            fields: {
                                keyword: {
                                    type: "keyword",
                                    ignore_above: 256,
                                },
                            },
                        },
                        relScore: {
                            type: "long",
                        },
                    },
                },
                mk: {
                    type: "nested",
                },
                nd: {
                    type: "nested",
                    properties: {
                        ndId: {
                            type: "text",
                            fields: {
                                keyword: {
                                    type: "keyword",
                                    ignore_above: 256,
                                },
                            },
                        },
                        relScore: {
                            type: "long",
                        },
                    },
                },
            },
            //     },
            // },
        },
    };

    try {
        const response = await client.indices.putMapping({
            index: newMapping.index,
            // type: '_doc',
            body: newMapping.payload,
        });
        console.log(response);
    } catch (error) {
        console.trace(error.message);
    }
}

(async function _start() {
    // try {
    //     const response = await client.indices.getMapping({
    //         index: "partans",
    //     });
    //     console.log(response);
    // } catch (error) {
    //     console.trace(error.message);
    // }

    // await client.indices.create({
    //     index: "partans",
    //     body: {
    //         settings: {
    //             index: {
    //                 number_of_replicas: 0, // for local development
    //             },
    //         },
    //     },
    // });

    // await putMapping();
})();
