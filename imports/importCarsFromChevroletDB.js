const __modifs = require("../data sources/fulldatafromdb.json");
const carMake = require("../mongoose/models/carMake");
const carModel = require("../mongoose/models/carModel");
const carModif = require("../mongoose/models/carModif");
const partNodeGroup = require("../mongoose/models/partNodeGroup");
const partNode = require("../mongoose/models/partNode");
const genPart = require("../mongoose/models/genPart");
const partan = require("../mongoose/models/partan");
const diagram = require("../mongoose/models/diagram");
const Part = require("../mongoose/models/part");
// ************************************************************
// ************************************************************

function clearStringForNeo(str) {
    const newStr = str.replace(/['"]+/g, "").toLowerCase();

    return newStr;
}

// ************************************************************
// ************************************************************
function extractMK(mk, modif) {
    mk.name = clearStringForNeo(modif.Name.split(" ")[0].trim());
    mk.tags = [modif.Name.split(" ")[1].trim()];
    mk.meta = {
        fromDatabase: modif,
        algorithms: {},
    };
}
// ************************************************************
// ************************************************************
function extractMD(md, modif) {
    md.name = clearStringForNeo(modif.md_name.split("(")[0].trim());
    md.tags = [modif.md_name.split("[")[1].split("]")[0].trim(), modif.md_code];
    if (modif.md_type) {
        md.tags.push(modif.md_type);
    }

    md.yearFrom = modif.YearFrom;
    md.yearTo = modif.YearTo;

    md.meta = {
        fromDatabase: modif,
        algorithms: {},
    };
}
// ************************************************************
// ************************************************************
function extractMF(mf, modif) {
    mf.name = clearStringForNeo(
        modif.md_name.split("(")[1].split(")")[0].trim()
    );
    mf.year = modif.DataRange;
    mf.models = {};
    mf.tags = [];

    mf.meta = {
        fromDatabase: modif,
        algorithms: {},
    };
}
// ************************************************************
// ************************************************************
function extractGN(gn, modif) {
    gn.name = clearStringForNeo(modif.gn_name);
    gn.tags = [];
    gn.syns = [];
    gn.positionalTags = [];
    gn.units = [];

    gn.meta = {
        fromDatabase: modif,
        algorithms: {},
    };
}
// ************************************************************
// ************************************************************
function extractND(nd, modif) {
    nd.name = clearStringForNeo(modif.nd_name.split("(")[0].trim());
    nd.tags = [];
    nd.syns = [];
    nd.positionalTags = [];
    nd.units = [];

    nd.meta = {
        fromDatabase: modif,
        algorithms: {},
    };
}
// ************************************************************
// ************************************************************
function extractGP(gp, modif) {
    gp.name = clearStringForNeo(modif.gp_name.split("(")[0].trim());
    gp.tags = [];
    gp.syns = [];
    gp.positionalTags = [];
    gp.units = [];
    gp.meta = {
        fromDatabase: modif,
        algorithms: {},
    };
}
// ************************************************************
// ************************************************************
function extractDGR(dgr, modif) {
    dgr.img = modif.Image;
    dgr.meta = {
        fromDatabase: modif,
        algorithms: {},
    };
}
// ************************************************************
// ************************************************************
function extractPART(part, modif) {
    part.bn = "GENERAL MOTORS";
    part.pn = modif.gp_code;
    part.desc = clearStringForNeo(modif.gp_name);
    part.isOrig = true;

    part.meta = {
        fromDatabase: modif,
        algorithms: {},
    };
}
// ************************************************************
// ************************************************************

function generatePTN(ptn, modif) {
    ptn.metaDesc = clearStringForNeo(
        `${modif.gp_name} ${modif.Name} ${modif.md_name} ${modif.DataRange}`
    );
    ptn.desc = clearStringForNeo(modif.gp_name);

    ptn.meta = {
        fromDatabase: modif,
        algorithms: {},
    };
}
// ************************************************************
// ************************************************************
function combine2Arrays(array1, array2) {
    if (!array1) {
        array1 = [];
    }
    for (let i in array2) {
        const x = array2[i];
        if (!array1.includes(x)) {
            array1.push(x);
        }
    }
}
// ************************************************************
// ************************************************************
async function upsertSimpleEntities(ent, Model, condition) {
    try {
        // console.log(
        //     "async function upsertCarEntities(ent, Model, condition) {",
        //     condition
        // );

        let dbEnt = await Model.findOne(condition);
        // console.log("dbEnt", dbEnt);
        if (!dbEnt) {
            console.log("creating new entity");
            dbEnt = new Model({
                ...ent,
            });
        } else {
            console.log("updating entity");

            if (!dbEnt.tags) {
                dbEnt.tags = [];
            }
            if (ent.tags) {
                combine2Arrays(dbEnt.tags, ent.tags);
            }
            if (!dbEnt.meta) {
                dbEnt.meta = ent.meta;
            }
            if (dbEnt.name !== ent.name) {
                if (!dbEnt.meta.otherNames) dbEnt.meta.otherNames = [];
                dbEnt.meta.otherNames.push(dbEnt.name);
                dbEnt.name = ent.name;
            }
            if (ent.nds) {
                dbEnt.nds = ent.nds;
            }
        }
        dbEnt = await dbEnt.save();
        // console.log(dbEnt._id);
        return dbEnt._id;
    } catch (e) {
        console.error("error");
        console.error(e);
        return null;
    }

    // ************************************************************
}

// ************************************************************
// ************************************************************
async function upsertPtn(ent, Model, condition) {
    console.log("async function upsertPtn(ent, Model, condition) {", condition);
    try {
        let dbEnt = await Model.findOne(condition);

        // console.log("dbEnt", dbEnt);
        if (!dbEnt) {
            console.log("creating new entity");
            dbEnt = new Model({
                ...ent,
            });
        } else {
            console.log("updating entity");

            if (!dbEnt.tags) {
                dbEnt.tags = [];
            }
            if (ent.tags) {
                combine2Arrays(dbEnt.tags, ent.tags);
            }
            if (!dbEnt.meta) {
                dbEnt.meta = ent.meta;
            }
            if (dbEnt.metaDesc !== ent.metaDesc) {
                if (!dbEnt.meta.otherNames) dbEnt.meta.otherNames = [];
                dbEnt.meta.otherNames.push(ent.name);
            }
            let mustInsertMf = true;
            for (let m in dbEnt.mf) {
                const mf = dbEnt.mf[m];
                if (mf._id.toString() == ent.mf[0]._id.toString()) {
                    mustInsertMf = false;
                    break;
                }
            }
            if (mustInsertMf) {
                dbEnt.mf.push(ent.mf[0]);
            }

            let mustInsertGp = true;
            for (let m in dbEnt.gp) {
                const gp = dbEnt.gp[m];
                if (gp._id.toString() == ent.gp[0]._id.toString()) {
                    mustInsertGp = false;
                    break;
                }
            }
            if (mustInsertGp) {
                dbEnt.gp.push(ent.gp[0]);
            }
        }
        dbEnt = await dbEnt.save();
        console.log(dbEnt._id);
        return dbEnt._id;
    } catch (e) {
        console.error(e);
        return null;
    }

    // ************************************************************
}

// ************************************************************
// ************************************************************
async function upsertDgr(ent, Model, condition) {
    console.log("async function upsertDgr(ent, Model, condition) {", condition);
    try {
        let dbEnt = await Model.findOne(condition);
        if (!dbEnt) {
            console.log("creating new entity");
            dbEnt = new Model({
                ...ent,
            });
        } else {
            console.log("updating entity");

            if (!dbEnt.tags) {
                dbEnt.tags = [];
            }
            if (ent.tags) {
                combine2Arrays(dbEnt.tags, ent.tags);
            }
            if (!dbEnt.meta) {
                dbEnt.meta = ent.meta;
            }

            let mustInsertMf = true;
            for (let m in dbEnt.mf) {
                const mf = dbEnt.mf[m];
                if (mf._id.toString() == ent.mf[0]._id.toString()) {
                    console.log("mf._id == ent.mf[0]._id they are equal");
                    mustInsertMf = false;
                    break;
                }
            }
            if (mustInsertMf) {
                dbEnt.mf.push(ent.mf[0]);
            }

            let mustInsertNd = true;
            for (let m in dbEnt.nd) {
                const nd = dbEnt.nd[m];

                if (nd._id.toString() == ent.nd[0]._id.toString()) {
                    mustInsertNd = false;
                    break;
                }
            }
            if (mustInsertNd) {
                dbEnt.nd.push(ent.nd[0]);
            }
        }
        // console.log("dbEnt", dbEnt);

        dbEnt = await dbEnt.save();
        console.log(dbEnt._id);
        return dbEnt._id;
    } catch (e) {
        console.error(e);
        return null;
    }

    // ************************************************************
}

// ************************************************************
// ************************************************************
async function upsertPart(ent, Model, condition) {
    console.log(
        "async function upsertPart(ent, Model, condition) {",
        condition
    );
    try {
        let dbEnt = await Model.findOne(condition);
        if (!dbEnt) {
            console.log("creating new entity");
            dbEnt = new Model({
                ...ent,
            });
        } else {
            console.log("updating entity");

            if (!dbEnt.tags) {
                dbEnt.tags = [];
            }
            if (ent.tags) {
                combine2Arrays(dbEnt.tags, ent.tags);
            }
            if (!dbEnt.meta) {
                dbEnt.meta = ent.meta;
            }

            let mustInsertPtn = true;
            for (let m in dbEnt.partan) {
                const partan = dbEnt.partan[m];
                
                if (partan._id.toString() == ent.partan[0]._id.toString()) {
                    mustInsertPtn = false;
                    break;
                }
            }
            if (mustInsertPtn) {
                dbEnt.partan.push(ent.partan[0]);
            }
        }
        // console.log("dbEnt", dbEnt);

        dbEnt = await dbEnt.save();
        console.log(dbEnt._id);
        return dbEnt._id;
    } catch (e) {
        console.error(e);
        return null;
    }

    // ************************************************************
}
// ************************************************************
// ************************************************************

function timeout(ms) {
    return new Promise((resolve) => setTimeout(resolve, ms));
}

async function exec() {
    try {
        console.log(
            "\n\n\n\n =============================================================== \n(async function exec() {"
        );
        console.log(__modifs.data.length);
        // return;
        const stepSize = 30;
        const maxCount = __modifs.data.length;
        const steps = maxCount / stepSize + 1;

        for (let is = 0; is < maxCount; is++) {
            let start = is * stepSize,
                end = (is + 1) * stepSize;
            console.log("start, end", start, end, maxCount);
            for (let ii in __modifs.data) {
                if (ii < start) continue;
                if (ii >= end) continue;
                console.log("for (let i in __modifs.data) {");

                modif = __modifs.data[ii];
                const mk = {},
                    md = {},
                    mf = {},
                    gn = {},
                    nd = {},
                    gp = {},
                    ptn = {},
                    dgr = {},
                    part = {};

                // ************************************************************
                extractMK(mk, modif);
                extractMD(md, modif);
                extractMF(mf, modif);

                extractGN(gn, modif);
                extractND(nd, modif);
                extractGP(gp, modif);

                extractDGR(dgr, modif);
                extractPART(part, modif);
                generatePTN(ptn, modif);

                // console.log(mk, md, mf, gn, nd, gp, dgr, part, ptn);
                // ************************************************************
                // upsert make
                const dbMakeId = await upsertSimpleEntities(mk, carMake, {
                    name: mk.name.toLowerCase(),
                });
                // console.log("dbMakeId - dbMakeId", dbMakeId);
                md.mks = {
                    _id: dbMakeId,
                    score: 100,
                };
                // console.log(3);
                // ************************************************************

                const split = md.name.split("+");
                for (let j in split) {
                    const mdName = split[j].trim();
                    md.name = mdName;
                    // ************************************************************
                    const dbModelId = await upsertSimpleEntities(md, carModel, {
                        name: md.name.toLowerCase(),
                        tags: md.tags,
                        "mks._id": dbMakeId,
                        "meta.fromDatabase.md_id": md.meta.fromDatabase.md_id,
                    });
                    mf.mds = {
                        _id: dbModelId,
                        score: 100,
                    };
                    // ************************************************************
                    const dbModifId = await upsertSimpleEntities(mf, carModif, {
                        // name: mf.name.toLowerCase(),
                        year: mf.year,
                        // // tags: mf.tags,
                        // "mds._id": dbModelId,
                        "meta.fromDatabase.mf_id": mf.meta.fromDatabase.mf_id,
                    });

                    // ************************************************************
                    gn.meta.fromDatabase.gnunique =
                        gn.meta.fromDatabase.gn_code;
                    const dbGnId = await upsertSimpleEntities(
                        gn,
                        partNodeGroup,
                        {
                            "meta.fromDatabase.gnunique":
                                gn.meta.fromDatabase.gnunique,
                        }
                    );
                    // ************************************************************
                    nd.gns = {
                        _id: dbGnId,
                        score: 100,
                    };
                    nd.meta.fromDatabase.ndunique = nd.meta.fromDatabase.nd_code.slice(
                        0,
                        3
                    );
                    // console.log(nd);
                    const dbNdId = await upsertSimpleEntities(nd, partNode, {
                        "meta.fromDatabase.ndunique":
                            nd.meta.fromDatabase.ndunique,
                    });

                    gp.nds = {
                        _id: dbNdId,
                        score: 100,
                    };

                    gp.meta.fromDatabase.gpunique = `${nd.meta.fromDatabase.ndunique}_${gp.meta.fromDatabase.PNC}`;
                    // ************************************************************
                    const dbGpId = await upsertSimpleEntities(gp, genPart, {
                        "meta.fromDatabase.gpunique":
                            gp.meta.fromDatabase.gpunique,
                    });
                    ptn.mf = [{ _id: dbModifId, score: 100 }];
                    ptn.gp = [{ _id: dbGpId, score: 100 }];

                    ptn.meta.fromDatabase.ptnunique = `${gp.meta.fromDatabase.gpunique}_${ptn.meta.fromDatabase.nd_id}`;

                    // ************************************************************
                    const ptnCondition = {
                        "mf._id": dbModifId,
                        "gp._id": dbGpId,
                        "meta.fromDatabase.ptnunique":
                            ptn.meta.fromDatabase.ptnunique,
                    };

                    const dbPtnId = await upsertPtn(ptn, partan, ptnCondition);
                    // ************************************************************

                    dgr.mf = [{ _id: dbModifId, score: 100 }];
                    dgr.nd = [{ _id: dbNdId, score: 100 }];

                    const dgrCondition = {
                        img: dgr.img,
                    };
                    const dbDgrId = await upsertDgr(dgr, diagram, dgrCondition);
                    // ************************************************************

                    part.partan = [{ _id: dbPtnId, relScore: 100 }];
                    const partCondition = {
                        pn: part.pn,
                        bn: part.bn,
                    };
                    const dbPartId = await upsertPart(
                        part,
                        Part,
                        partCondition
                    );
                    // return;
                }
                // console.log(mf);
                // if (ii > 1) return;
            }
            console.log("before timeout");
            console.log("start, end", start, end, maxCount);
            // return;
            await timeout(60 * 10 * 5);
            console.log("afterTimeOut timeout");
        }
    } catch (e) {
        console.log(e);
    }
}

module.exports.importCarsFromChevroletDB = exec;
