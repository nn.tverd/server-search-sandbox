var elasticsearch = require("elasticsearch");
const partanRe = require("./mongoose/models/partan");
var client = new elasticsearch.Client({
    host: "localhost:9200",
    // log: "trace",
    apiVersion: "7.2", // use the same version of your Elasticsearch instance
});

client.ping(
    {
        // ping usually has a 3000ms timeout
        requestTimeout: 5000,
    },
    function (error) {
        if (error) {
            console.trace("elasticsearch cluster is down!");
        } else {
            // console.log("All is well");
        }
    }
);

const deepCopy = (object) => JSON.parse(JSON.stringify(object));

const searchMakes = async (query) => {
    try {
        const makes = await client.search({
            index: "makes",
            q: query,
        });
        console.log("makes.hits.hits");
        console.log(makes.hits.hits);
        console.log("========================");
        return makes.hits.hits;
    } catch (error) {
        console.trace(error.message);
    }
};
const searchModels = async (query) => {
    try {
        const models = await client.search({
            index: "models",
            q: query,
        });
        console.log("models.hits.hits");
        console.log(models.hits.hits);
        console.log("========================");
        return models.hits.hits;
    } catch (error) {
        console.trace(error.message);
    }
};
const searchModifs = async (query) => {
    try {
        const modifs = await client.search({
            index: "modifs",
            q: query,
        });
        console.log("modifs.hits.hits");
        console.log(modifs.hits.hits);
        console.log("========================");
        return modifs.hits.hits;
    } catch (error) {
        console.trace(error.message);
    }
};

const searchPartNodeGroups = async (query) => {
    try {
        const partnodegroups = await client.search({
            index: "v3partnodegroups",
            q: query,
        });
        console.log("partnodegroups.hits.hits");
        console.log(partnodegroups.hits.hits);
        console.log("========================");
        return partnodegroups.hits.hits;
    } catch (error) {
        console.trace(error.message);
    }
};

const searchPartNodes = async (query) => {
    try {
        const partnodes = await client.search({
            index: "v3partnodes",
            q: query,
        });
        console.log("partnodes.hits.hits");
        console.log(partnodes.hits.hits);
        console.log("========================");
        return partnodes.hits.hits;
    } catch (error) {
        console.trace(error.message);
    }
};

const searchGenParts = async (query) => {
    console.log(query);
    try {
        const genparts = await client.search({
            index: "v3genparts",
            q: query,
        });
        console.log("genparts.hits.hits");
        console.log(genparts.hits.hits);
        console.log("========================");
        return genparts.hits.hits;
    } catch (error) {
        console.trace(error.message);
    }
};

const searchPartan = async (_query, _scores, _sq, _filters) => {
    try {
        const report = [];
        const mkSearch = [];
        for (let i in _sq.makes) {
            const kboost = _filters.makes.includes(_sq.makes[i].id) ? 10 : 1;
            const item = {
                terms: {
                    "mk.id": [_sq.makes[i].id],
                    boost: _sq.makes[i].score * kboost,
                },
            };
            mkSearch.push(JSON.parse(JSON.stringify(item)));
            item.terms.name = _sq.makes[i].name;
            report.push(item);
            console.log("mkSearch", mkSearch);
        }
        const mdSearch = [];
        for (let i in _sq.models) {
            const kboost = _filters.models.includes(_sq.models[i].id) ? 10 : 1;
            const item = {
                terms: {
                    "md.id": [_sq.models[i].id],
                    boost: _sq.models[i].score * kboost,
                },
            };
            mdSearch.push(JSON.parse(JSON.stringify(item)));
            item.terms.name = _sq.models[i].name;
            report.push(item);
        }
        console.log("mdSearch", mdSearch);

        const mfSearch = [];
        for (let i in _sq.modifs) {
            const kboost = _filters.modifs.includes(_sq.modifs[i].id) ? 10 : 1;
            const item = {
                terms: {
                    "mf.id": [_sq.modifs[i].id],
                    boost: _sq.modifs[i].score * kboost,
                },
            };
            mfSearch.push(JSON.parse(JSON.stringify(item)));
            item.terms.name = _sq.modifs[i].name;
            report.push(item);
        }
        console.log("mfSearch", mfSearch);

        const gnSearch = [];
        for (let i in _sq.v3partnodegroups) {
            const kboost = _filters.partNodeGroups.includes(
                _sq.v3partnodegroups[i].id
            )
                ? 10
                : 1;
            const item = {
                terms: {
                    "gn.id": [_sq.v3partnodegroups[i].id],
                    boost: _sq.v3partnodegroups[i].score * kboost,
                },
            };
            gnSearch.push(JSON.parse(JSON.stringify(item)));
            item.terms.name = _sq.v3partnodegroups[i].name;
            report.push(item);
        }
        console.log("gnSearch", gnSearch);

        const ndSearch = [];
        for (let i in _sq.v3partnodes) {
            const kboost = _filters.partNodes.includes(_sq.v3partnodes[i].id)
                ? 10
                : 1;
            const item = {
                terms: {
                    "nd.id": [_sq.v3partnodes[i].id],
                    boost: _sq.v3partnodes[i].score * kboost,
                },
            };
            ndSearch.push(JSON.parse(JSON.stringify(item)));
            item.terms.name = _sq.v3partnodes[i].name;
            report.push(item);
        }
        console.log("ndSearch", ndSearch);

        const gpSearch = [];
        for (let i in _sq.v3genparts) {
            const kboost = _filters.genParts.includes(_sq.v3genparts[i].id)
                ? 10
                : 1;
            const item = {
                terms: {
                    "gp.id": [_sq.v3genparts[i].id],
                    boost: _sq.v3genparts[i].score * kboost,
                },
            };
            gpSearch.push(JSON.parse(JSON.stringify(item)));
            item.terms.name = _sq.v3genparts[i].name;
            report.push(item);
        }
        console.log("gpSearch", gpSearch);
        console.log("report", JSON.stringify(report));

        const body5 = {
            query: {
                bool: {
                    should: [
                        {
                            nested: {
                                path: "md",
                                query: {
                                    bool: {
                                        should: mdSearch,
                                    },
                                },
                                // boost: _scores.models,
                            },
                        },
                        {
                            nested: {
                                path: "mk",
                                query: {
                                    bool: {
                                        should: mkSearch,
                                    },
                                },
                                boost: _scores.makes,
                            },
                        },
                        {
                            nested: {
                                path: "mf",
                                query: {
                                    bool: {
                                        should: mfSearch,
                                    },
                                },
                                boost: _scores.modifs,
                            },
                        },
                        {
                            nested: {
                                path: "gp",
                                query: {
                                    bool: {
                                        should: gpSearch,
                                    },
                                },
                                boost: _scores.genparts,
                            },
                        },
                        {
                            nested: {
                                path: "gn",
                                query: {
                                    bool: {
                                        should: gnSearch,
                                    },
                                },
                                boost: _scores.partnodegroups,
                            },
                        },
                        {
                            nested: {
                                path: "nd",
                                query: {
                                    bool: {
                                        should: ndSearch,
                                    },
                                },
                                boost: _scores.partnodes,
                            },
                        },
                    ],
                },
            },
        };
        const partans = await client.search({
            index: "partans",
            body: body5,
        });
        console.log("partans.hits.hits", partans.hits.hits.length);
        for (let i in partans.hits.hits) {
            console.log(
                "partans.hits.hits[i]._source",
                partans.hits.hits[i]._source.metaDesc
            );
            console.log(
                "partans.hits.hits[i]._score",
                partans.hits.hits[i]._score
            );
        }
        console.log("========================");
        return partans.hits.hits;
    } catch (error) {
        console.trace(error.message);
    }
};

const createObjFromESHits = (hits, object) => {
    for (let i in hits) {
        const hit = hits[i];
        if (!object[hit._index]) {
            object[hit._index] = {};
        }
        if (object[hit._index][hit._id])
            object[hit._index][hit._id]["_score"] += hit._score;
        else object[hit._index][hit._id] = JSON.parse(JSON.stringify(hit));
    }
};

const updateRelationsScores = (object) => {
    for (let i in object) {
        const index = object[i];
        for (let j in index) {
            const hit = index[j];
            if (hit.isScoreUpdated) continue;
            hit._score = hit._score * hit._source.score;
            hit.isScoreUpdated = true;
            console.log(
                "relations score is updated",
                hit._score,
                hit._source.score
            );
        }
    }
};

const searchPartan2 = async (relations, _filters) => {
    try {
        // const indexToSearch = [];
        const searchQuery = {};
        for (let i in relations) {
            // const kboost = _filters.makes.includes(_sq.makes[i].id) ? 10 : 1;
            const rel = relations[i];
            for (let j in rel) {
                const hit = rel[j];

                const source = hit._source;
                let subIndx1 = "make";
                let subIndx2 = "mk";
                let subId = "";
                if (source.aType == "partanRel") {
                    subIndx1 = source.bType;
                    subId = source.aId;
                }
                if (source.bType == "partanRel") {
                    subIndx1 = source.aType;
                    subId = source.bId;
                }
                switch (subIndx1) {
                    case "make": {
                        subIndx2 = "mk";
                        break;
                    }
                    case "model": {
                        subIndx2 = "md";
                        break;
                    }
                    case "modif": {
                        subIndx2 = "mf";
                        break;
                    }
                    case "gp": {
                        subIndx2 = "gp";
                        break;
                    }
                    case "nd": {
                        subIndx2 = "nd";
                        break;
                    }
                    case "gn": {
                        subIndx2 = "gn";
                        break;
                    }
                    default: {
                        subIndx2 = null;
                    }
                }
                const item = {
                    terms: {
                        [`${subIndx2}.id`]: [subId],
                        boost: hit._score,
                    },
                };
                if (!searchQuery[subIndx2]) searchQuery[subIndx2] = [];

                searchQuery[subIndx2].push(JSON.parse(JSON.stringify(item)));
            }
        }

        console.log("searchQuery", searchQuery);
       
        const body6 = {
            query: {
                bool: {
                    should: [],
                },
            },
        };
        for (let i in searchQuery) {
            const subIndx2 = i;
            body6.query.bool.should.push({
                nested: {
                    path: subIndx2,
                    query: {
                        bool: {
                            should: searchQuery[subIndx2],
                        },
                    },
                    // boost: _score.models,
                },
            });
        }
        console.log("body6", JSON.stringify(body6));
        // return;

        const partans = await client.search({
            index: "partans",
            body: body6,
        });
        console.log("partans.hits.hits", partans.hits.hits.length);
        for (let i in partans.hits.hits) {
            console.log(
                "partans.hits.hits[i]._source",
                partans.hits.hits[i]._source.metaDesc
            );
            console.log(
                "partans.hits.hits[i]._score",
                partans.hits.hits[i]._score
            );
        }
        console.log("========================");
        return partans.hits.hits;
    } catch (error) {
        console.trace(error.message);
    }
};

async function _start2(queryString, filters) {
    console.log("elasticSearchApi2.js async function _start2");

    const q = queryString;
    console.log(q);
    // const q = "КОМПЛЕКТ ПЕРЕДНИХ КОЛОДОК chevrolet aveo 2011";
    const _makes = searchMakes(q);
    const _models = searchModels(q);
    const _modifs = searchModifs(q);
    const _partNodeGroups = searchPartNodeGroups(q);
    const _partNodes = searchPartNodes(q);
    const _genParts = searchGenParts(q);

    //*****************************************************************************
    //*****************************************************************************
    //*****************************************************************************
    //*****************************************************************************
    const makes = await _makes;
    const models = await _models;
    const modifs = await _modifs;
    const partNodeGroups = await _partNodeGroups;
    const partNodes = await _partNodes;
    const genParts = await _genParts;

    const firstSearchObject = {};
    createObjFromESHits(makes, firstSearchObject);
    createObjFromESHits(models, firstSearchObject);
    createObjFromESHits(modifs, firstSearchObject);
    createObjFromESHits(partNodeGroups, firstSearchObject);
    createObjFromESHits(partNodes, firstSearchObject);
    createObjFromESHits(genParts, firstSearchObject);

    console.log(firstSearchObject);

    const ids = [];

    // +++++++++++++++++++++++++++++++++++++++++
    (function getIds2(gp, nd, mf, md, ids) {
        const _ids = {};
        // genparts
        for (let i in gp) {
            const item = gp[i];
            if (!_ids[item._source.partNodeGroupId]) {
                _ids[item._source.partNodeGroupId] = {
                    id: item._source.partNodeGroupId,
                    score: item._score,
                };
            } else {
                _ids[item._source.partNodeGroupId].score += item._score;
            }

            if (!_ids[item._source.partNodeId]) {
                _ids[item._source.partNodeId] = {
                    id: item._source.partNodeId,
                    score: item._score,
                };
            } else {
                _ids[item._source.partNodeId].score += item._score;
            }

            // ids.push(item._source.partNodeGroupId);
            // ids.push(item._source.partNodeId);
        }
        // modes
        for (let i in nd) {
            const item = gp[i];
            // ids.push(item._source.partNodeGroupId);
            if (!_ids[item._source.partNodeGroupId]) {
                _ids[item._source.partNodeGroupId] = {
                    id: item._source.partNodeGroupId,
                    score: item._score,
                };
            } else {
                _ids[item._source.partNodeGroupId].score += item._score;
            }
        }
        // modifs
        for (let i in mf) {
            const item = mf[i];
            if (!_ids[item._source.makeId]) {
                _ids[item._source.makeId] = {
                    id: item._source.makeId,
                    score: item._score,
                };
            } else {
                _ids[item._source.makeId].score += item._score;
            }

            if (!_ids[item._source.modelId]) {
                _ids[item._source.modelId] = {
                    id: item._source.modelId,
                    score: item._score,
                };
            } else {
                _ids[item._source.modelId].score += item._score;
            }

            // ids.push(item._source.partNodeGroupId);
            // ids.push(item._source.partNodeId);
        }
        // models
        for (let i in md) {
            const item = md[i];
            if (!_ids[item._source.makeId]) {
                _ids[item._source.makeId] = {
                    id: item._source.makeId,
                    score: item._score,
                };
            } else {
                _ids[item._source.makeId].score += item._score;
            }
        }
        for (let i in _ids) {
            ids.push(_ids[i]);
        }
    })(await genParts, await partNodes, await modifs, await models, ids);
    // *****************************************************************************
    // reverse search
    console.log("ids", ids);
    // GET /_search
    const idsQ = {
        query: {
            bool: {
                should: [],
            },
        },
    };
    for (let i in ids) {
        idsQ.query.bool.should.push({
            term: {
                _id: {
                    value: ids[i].id,
                    boost: ids[i].score,
                },
            },
        });
    }
    console.log(JSON.stringify(idsQ));
    const aditionalSearch = await client.search({
        index: ["v3partnodes", "v3partnodegroups", "makes", "models"],
        // index: "v3partnodes",
        body: idsQ,
    });
    console.log("aditionalSearch", aditionalSearch.hits.hits);
    // *****************************************************************************

    const secondSearchObject = deepCopy(firstSearchObject);
    createObjFromESHits(aditionalSearch.hits.hits, secondSearchObject);

    console.log("secondSearchObject", secondSearchObject);

    // I have to got all the relations

    const idsForRel = {
        query: {
            bool: {
                should: [],
            },
        },
    };
    for (let i in secondSearchObject) {
        const index = secondSearchObject[i];
        for (let j in index) {
            const hit = index[j];
            idsForRel.query.bool.should.push({
                term: {
                    aId: {
                        value: hit._id,
                        boost: hit._score,
                    },
                },
            });
            idsForRel.query.bool.should.push({
                term: {
                    bId: {
                        value: hit._id,
                        boost: hit._score,
                    },
                },
            });
        }
    }
    console.log("idsForRel", JSON.stringify(idsForRel));
    const relations = await client.search({
        index: ["relations"],
        // index: "v3partnodes",
        body: idsForRel,
    });

    const relationSearchObject = {};
    createObjFromESHits(relations.hits.hits, relationSearchObject);

    console.log("relationSearchObject", relationSearchObject);

    // Update score of relations

    updateRelationsScores(relationSearchObject);

    //*****************************************************************************
    //*****************************************************************************
    //*****************************************************************************

    const partans = await searchPartan2(relationSearchObject);
    return;
    // console.log(partans);

    // ------------ cars extraction from partans ++++++++++++++++++++++++++++++++++

    function updateCarsResults(partans) {
        let carEntIds = [];
        for (let e in partans) {
            // if(e>1) continue;
            const partan = partans[e];
            let ptnIds = [];
            // console.log("function updateCarsResults(partans) {", partan);
            ptnIds = ptnIds.concat(partan._source.mk);
            ptnIds = ptnIds.concat(partan._source.md);
            ptnIds = ptnIds.concat(partan._source.mf);
            // console.log("partan._score", partan._score);
            for (let idx in ptnIds) {
                if (ptnIds[idx].relScore > 0) {
                    ptnIds[idx].relScore =
                        (ptnIds[idx].relScore + partan._score) * 0.0005;
                } else {
                    ptnIds[idx].relScore = 0;
                }
            }
            carEntIds = carEntIds.concat(JSON.parse(JSON.stringify(ptnIds)));
        }
        return JSON.parse(JSON.stringify(carEntIds));
    }
    const ptnsCarIds = updateCarsResults(partans);
    const idsQ2 = {
        query: {
            bool: {
                should: [],
            },
        },
    };
    for (let i in ptnsCarIds) {
        idsQ2.query.bool.should.push({
            term: {
                _id: {
                    value: ptnsCarIds[i].id,
                    boost: ptnsCarIds[i].relScore,
                },
            },
        });
    }
    // console.log("\n\n\n\nJSON.stringify(idsQ2)", JSON.stringify(idsQ2));
    const carUpdatesResults = await client.search({
        index: ["modifs", "makes", "models"],
        // index: "v3partnodes",
        body: idsQ2,
    });

    console.log("carUpdatesResults", carUpdatesResults.hits.hits);

    // **********************************************************
    const ___modifs = {};
    for (let i in carUpdatesResults.hits.hits) {
        const hit = carUpdatesResults.hits.hits[i];
        if (hit._index === "makes") {
            if (!___makes[hit._id]) {
                ___makes[hit._id] = {
                    ...hit,
                };
            } else {
                ___makes[hit._id]._score += hit._score;
            }
        }
        if (hit._index === "models") {
            if (!___models[hit._id]) {
                ___models[hit._id] = {
                    ...hit,
                };
            } else {
                ___models[hit._id]._score += hit._score;
            }
        }
        if (hit._index === "modifs") {
            if (!___modifs[hit._id]) {
                ___modifs[hit._id] = {
                    ...hit,
                };
            } else {
                ___modifs[hit._id]._score += hit._score;
            }
        }
    }

    const _sendedModifs = [];
    for (let i in ___modifs) {
        _sendedModifs.push(___modifs[i]);
    }
    const _sendedMakes = [];
    for (let i in ___makes) {
        _sendedMakes.push(___makes[i]);
    }
    const _sendedModels = [];
    for (let i in ___models) {
        _sendedModels.push(___models[i]);
    }
    console.log(_sendedMakes, _sendedModels, _sendedModifs);
    // ------------ cars extraction from partans ----------------------------------

    return {
        partans: partans,
        // makes: await makes,
        // models: await models,
        makes: _sendedMakes,
        models: _sendedModels,
        modifs: _sendedModifs,
        // partNodeGroups: await partNodeGroups,
        // partNodes: await partNodes,
        partNodeGroups: sendedPartNodeGroups,
        partNodes: sendedPartNodes,
        genParts: await genParts,
    };
}

module.exports.start = _start2;
