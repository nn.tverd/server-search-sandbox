var elasticsearch = require("elasticsearch");
var client = new elasticsearch.Client({
    host: "localhost:9200",
    log: "trace",
    apiVersion: "7.2", // use the same version of your Elasticsearch instance
});

client.ping(
    {
        // ping usually has a 3000ms timeout
        requestTimeout: 5000,
    },
    function (error) {
        if (error) {
            console.trace("elasticsearch cluster is down!");
        } else {
            console.log("All is well");
        }
    }
);

const searchFunction = async (query) => {
    try {
        const response = await client.search({
            q: query,
        });
        console.log(response.hits.hits);
    } catch (error) {
        console.trace(error.message);
    }
};

async function insertFunction(item, index) {
    try {
        console.log(item, index);
        const id = item._id;
        delete item._id;
        const response = await client.index({
            index: index,
            id: id,
            body: item,
        });

        console.log(response);
    } catch (error) {
        console.trace(error.message);
    }
}

(async function _start() {
    const makes = [{ _id: "5f8e7244a93b28103383252e", name: "CHEVROLET" }];
    const models = [
        {
            _id: "5f8e727262b1144467547ac2",
            name: "AVEO",
            makeId: "5f8e7244a93b28103383252e",
        },
        {
            _id: "5f8e732c62b1144467547ac3",
            name: "EPICA",
            makeId: "5f8e7244a93b28103383252e",
        },
    ];

    const modifs = [
        {
            _id: "5f8e736f62b1144467547ac5",
            name: "T225",
            year: "2011",
            modelId: "5f8e727262b1144467547ac2",
            makeId: "5f8e7244a93b28103383252e",
        },
        {
            _id: "5f8e739c62b1144467547ac6",
            name: "T200",
            year: "2003",
            modelId: "5f8e727262b1144467547ac2",
            makeId: "5f8e7244a93b28103383252e",
        },
        {
            _id: "5f8e73cb62b1144467547ac7",
            name: "V250",
            year: "2011",
            modelId: "5f8e732c62b1144467547ac3",
            makeId: "5f8e7244a93b28103383252e",
        },
    ];

    const partNodeGroups = [
        {
            _id: "5f8e7da362b1144467547ac8",
            name: "КУЗОВ И ЕГО НАРУЖНЫЕ ЭЛЕМЕНТЫ",
        },
        {
            _id: "5f8e7dd162b1144467547ac9",
            name: "ШАССИ",
        },
    ];

    const partNodes = [
        {
            _id: "5f8e7def62b1144467547aca",
            name: "БАМПЕР",
            positionalTags: ["передний"],
            partNodeGroupId: "5f8e7da362b1144467547ac8",
        },
        {
            _id: "5f8e7e0a62b1144467547acb",
            name: "БАМПЕР",
            positionalTags: ["задний"],
            partNodeGroupId: "5f8e7da362b1144467547ac8",
        },
        {
            _id: "5f8e7e2262b1144467547acc",
            name: "ТОРМОЗ",
            positionalTags: ["передний"],
            partNodeGroupId: "5f8e7dd162b1144467547ac9",
        },
    ];

    const genParts = [
        {
            _id: "5f8eba6a62b1144467547acd",
            name: "БАМПЕР",
            positionalTags: ["передний"],
            units: ["бампер", "штука"],
            partNodeId: "5f8e7def62b1144467547aca",
            partNodeGroupId: "5f8e7da362b1144467547ac8",
        },
        {
            _id: "5f8eba8862b1144467547ace",
            name: "БАМПЕР",
            positionalTags: ["задний"],
            units: ["бампер", "штука"],
            partNodeId: "5f8e7e0a62b1144467547acb",
            partNodeGroupId: "5f8e7da362b1144467547ac8",
        },
        {
            _id: "5f8ebaa162b1144467547acf",
            name: "КОЛОДКИ",
            positionalTags: ["передние"],
            tags: ["дисковые тормоза", "тормоза на диски"],
            units: ["бампер", "штука"],
            partNodeId: "5f8e7e2262b1144467547acc",
            partNodeGroupId: "5f8e7dd162b1144467547ac9",
        },
    ];

    const partans = [
        {
            _id: "5f8f32c6e20bd2070fd9fe22",
            mk: [
                {
                    mkId: "5f8e7244a93b28103383252e",
                    relScore: 100,
                },
            ],
            md: [
                {
                    mdId: "5f8e727262b1144467547ac2",
                    relScore: 100,
                },
            ],
            mf: [
                {
                    mfId: "5f8e739c62b1144467547ac6",
                    relScore: 100,
                },
            ],
            gp: [
                {
                    gp: "5f8eba6a62b1144467547acd",
                    relScore: 100,
                },
            ],
            nd: [
                {
                    ndId: "5f8e7def62b1144467547aca",
                    relScore: 100,
                },
            ],
            gn: [
                {
                    gnId: "5f8e7da362b1144467547ac8",
                    relScore: 100,
                },
            ],
            desc: "передний бампер",
            metaDesc: "передний бампер шевроле авео 2003 года",
            imagesUrl: [
                {
                    iUrl: "google.com",
                    relScore: 100,
                },
            ],
        },
        {
            _id: "5f91262946cd8821bedf1a14",
            mk: [
                {
                    mkId: "5f8e7244a93b28103383252e",
                    relScore: 100,
                },
            ],
            md: [
                {
                    mdId: "5f8e727262b1144467547ac2",
                    relScore: 100,
                },
            ],
            mf: [
                {
                    mfId: "5f8e739c62b1144467547ac6",
                    relScore: 100,
                },
            ],
            gp: [
                {
                    gp: "5f8eba8862b1144467547ace",
                    relScore: 100,
                },
            ],
            nd: [
                {
                    ndId: "5f8e7e0a62b1144467547acb",
                    relScore: 100,
                },
            ],
            gn: [
                {
                    gnId: "5f8e7da362b1144467547ac8",
                    relScore: 100,
                },
            ],
            desc: "задний бампер",
            metaDesc: "задний бампер шевроле авео 2003 года",
            imagesUrl: [
                {
                    iUrl: "google.com",
                    relScore: 100,
                },
            ],
        },
        {
            _id: "5f91265246cd8821bedf1a15",
            mk: [
                {
                    mkId: "5f8e7244a93b28103383252e",
                    relScore: 100,
                },
            ],
            md: [
                {
                    mdId: "5f8e727262b1144467547ac2",
                    relScore: 100,
                },
            ],
            mf: [
                {
                    mfId: "5f8e739c62b1144467547ac6",
                    relScore: 100,
                },
            ],
            gp: [
                {
                    gp: "5f8ebaa162b1144467547acf",
                    relScore: 100,
                },
            ],
            nd: [
                {
                    ndId: "5f8e7e2262b1144467547acc",
                    relScore: 100,
                },
            ],
            gn: [
                {
                    gnId: "5f8e7dd162b1144467547ac9",
                    relScore: 100,
                },
            ],
            desc: "тормозные колодки передние",
            metaDesc: "Тормозные колодки передние шевроле авео 2003 года",
            imagesUrl: [
                {
                    iUrl: "google.com",
                    relScore: 100,
                },
            ],
        },
        {
            _id: "5f91265c46cd8821bedf1a16",
            mk: [
                {
                    mkId: "5f8e7244a93b28103383252e",
                    relScore: 100,
                },
            ],
            md: [
                {
                    mdId: "5f8e727262b1144467547ac2",
                    relScore: 100,
                },
            ],
            mf: [
                {
                    mfId: "5f8e736f62b1144467547ac5",
                    relScore: 100,
                },
            ],
            gp: [
                {
                    gp: "5f8eba6a62b1144467547acd",
                    relScore: 100,
                },
            ],
            nd: [
                {
                    ndId: "5f8e7def62b1144467547aca",
                    relScore: 100,
                },
            ],
            gn: [
                {
                    gnId: "5f8e7da362b1144467547ac8",
                    relScore: 100,
                },
            ],
            desc: "передний бампер",
            metaDesc: "передний бампер шевроле авео 2011 года",
            imagesUrl: [
                {
                    iUrl: "google.com",
                    relScore: 100,
                },
            ],
        },
        {
            _id: "5f91266a46cd8821bedf1a17",
            mk: [
                {
                    mkId: "5f8e7244a93b28103383252e",
                    relScore: 100,
                },
            ],
            md: [
                {
                    mdId: "5f8e727262b1144467547ac2",
                    relScore: 100,
                },
            ],
            mf: [
                {
                    mfId: "5f8e736f62b1144467547ac5",
                    relScore: 100,
                },
            ],
            gp: [
                {
                    gp: "5f8eba8862b1144467547ace",
                    relScore: 100,
                },
            ],
            nd: [
                {
                    ndId: "5f8e7e0a62b1144467547acb",
                    relScore: 100,
                },
            ],
            gn: [
                {
                    gnId: "5f8e7da362b1144467547ac8",
                    relScore: 100,
                },
            ],
            desc: "задний бампер",
            metaDesc: "задний бампер шевроле авео 2011 года",
            imagesUrl: [
                {
                    iUrl: "google.com",
                    relScore: 100,
                },
            ],
        },
        {
            _id: "5f91267446cd8821bedf1a18",
            mk: [
                {
                    mkId: "5f8e7244a93b28103383252e",
                    relScore: 100,
                },
            ],
            md: [
                {
                    mdId: "5f8e727262b1144467547ac2",
                    relScore: 100,
                },
            ],
            mf: [
                {
                    mfId: "5f8e736f62b1144467547ac5",
                    relScore: 100,
                },
            ],
            gp: [
                {
                    gp: "5f8ebaa162b1144467547acf",
                    relScore: 100,
                },
            ],
            nd: [
                {
                    ndId: "5f8e7e2262b1144467547acc",
                    relScore: 100,
                },
            ],
            gn: [
                {
                    gnId: "5f8e7dd162b1144467547ac9",
                    relScore: 100,
                },
            ],
            desc: "тормозные колодки передние",
            metaDesc: "Тормозные колодки передние шевроле авео 2011 года",
            imagesUrl: [
                {
                    iUrl: "google.com",
                    relScore: 100,
                },
            ],
        },
        {
            _id: "5f91268b46cd8821bedf1a19",
            mk: [
                {
                    mkId: "5f8e7244a93b28103383252e",
                    relScore: 100,
                },
            ],
            md: [
                {
                    mdId: "5f8e732c62b1144467547ac3",
                    relScore: 100,
                },
            ],
            mf: [
                {
                    mfId: "5f8e73cb62b1144467547ac7",
                    relScore: 100,
                },
            ],
            gp: [
                {
                    gp: "5f8eba6a62b1144467547acd",
                    relScore: 100,
                },
            ],
            nd: [
                {
                    ndId: "5f8e7def62b1144467547aca",
                    relScore: 100,
                },
            ],
            gn: [
                {
                    gnId: "5f8e7da362b1144467547ac8",
                    relScore: 100,
                },
            ],
            desc: "передний бампер",
            metaDesc: "передний бампер шевроле эпика 2011 года",
            imagesUrl: [
                {
                    iUrl: "google.com",
                    relScore: 100,
                },
            ],
        },
        {
            _id: "5f91269246cd8821bedf1a1a",
            mk: [
                {
                    mkId: "5f8e7244a93b28103383252e",
                    relScore: 100,
                },
            ],
            md: [
                {
                    mdId: "5f8e732c62b1144467547ac3",
                    relScore: 100,
                },
            ],
            mf: [
                {
                    mfId: "5f8e73cb62b1144467547ac7",
                    relScore: 100,
                },
            ],
            gp: [
                {
                    gp: "5f8eba8862b1144467547ace",
                    relScore: 100,
                },
            ],
            nd: [
                {
                    ndId: "5f8e7e0a62b1144467547acb",
                    relScore: 100,
                },
            ],
            gn: [
                {
                    gnId: "5f8e7da362b1144467547ac8",
                    relScore: 100,
                },
            ],
            desc: "задний бампер",
            metaDesc: "задний бампер шевроле эпика 2011 года",
            imagesUrl: [
                {
                    iUrl: "google.com",
                    relScore: 100,
                },
            ],
        },
        {
            _id: "5f9126b446cd8821bedf1a1b",
            mk: [
                {
                    mkId: "5f8e7244a93b28103383252e",
                    relScore: 100,
                },
            ],
            md: [
                {
                    mdId: "5f8e732c62b1144467547ac3",
                    relScore: 100,
                },
            ],
            mf: [
                {
                    mfId: "5f8e73cb62b1144467547ac7",
                    relScore: 100,
                },
            ],
            gp: [
                {
                    gp: "5f8ebaa162b1144467547acf",
                    relScore: 100,
                },
            ],
            nd: [
                {
                    ndId: "5f8e7e2262b1144467547acc",
                    relScore: 100,
                },
            ],
            gn: [
                {
                    gnId: "5f8e7dd162b1144467547ac9",
                    relScore: 100,
                },
            ],
            desc: "тормозные колодки передние",
            metaDesc: "Тормозные колодки передние шевроле эпика 2011 года",
            imagesUrl: [
                {
                    iUrl: "google.com",
                    relScore: 100,
                },
            ],
        },
    ];

    const parts = [
        {
            _id: "5f8edb6fa93b281033832543",
            bn: "GENERAL MOTORS",
            pn: "93744489",
            desc: "Бампер передний",
            isOrig: true,
            imagesUlr: ["http://www.google.com"],
            partan: [
                {
                    ptnId: "5f91268b46cd8821bedf1a19",
                    relScore: 100,
                },
            ],
        },
        {
            _id: "5f8edb8ba93b281033832544",
            bn: "GENERAL MOTORS",
            pn: "96842666",
            desc: "Бампер передний",
            isOrig: true,
            imagesUlr: ["http://www.google.com"],
            partan: [
                {
                    ptnId: "5f91268b46cd8821bedf1a19",
                    relScore: 100,
                },
            ],
        },
        {
            _id: "5f8edbc2a93b281033832545",
            bn: "GENERAL MOTORS",
            pn: "93745339",
            desc: "Бампер задний",
            isOrig: true,
            imagesUlr: ["http://www.google.com"],
            partan: [
                {
                    ptnId: "5f91269246cd8821bedf1a1a",
                    relScore: 100,
                },
            ],
        },
        {
            _id: "5f8edc5aa93b281033832546",
            bn: "GENERAL MOTORS",
            pn: "93745100",
            desc: "Бампер задний",
            isOrig: true,
            imagesUlr: ["http://www.google.com"],
            partan: [
                {
                    ptnId: "5f91269246cd8821bedf1a1a",
                    relScore: 100,
                },
            ],
        },
        {
            _id: "5f8edc8ca93b281033832547",
            bn: "GENERAL MOTORS",
            pn: "96873740",
            desc: "Бампер задний",
            isOrig: true,
            imagesUlr: ["http://www.google.com"],
            partan: [
                {
                    ptnId: "5f91269246cd8821bedf1a1a",
                    relScore: 100,
                },
            ],
        },
        {
            _id: "5f8edc99a93b281033832548",
            bn: "GENERAL MOTORS",
            pn: "96953405",
            desc: "Бампер задний",
            isOrig: true,
            imagesUlr: ["http://www.google.com"],
            partan: [
                {
                    ptnId: "5f91269246cd8821bedf1a1a",
                    relScore: 100,
                },
            ],
        },
        {
            _id: "5f8edd31a93b281033832549",
            bn: "GENERAL MOTORS",
            pn: "96952179",
            desc: "Комплект колодок",
            isOrig: true,
            imagesUlr: ["http://www.google.com"],
            partan: [
                {
                    ptnId: "5f9126b446cd8821bedf1a1b",
                    relScore: 100,
                },
            ],
        },
        {
            _id: "5f8ebcbf62b1144467547ad0",
            bn: "GENERAL MOTORS",
            pn: "93740273",
            desc: "Бампер передний",
            isOrig: true,
            imagesUlr: ["http://www.google.com"],
            partan: [
                {
                    ptnId: "5f8f32c6e20bd2070fd9fe22",
                    relScore: 100,
                },
            ],
        },
        {
            _id: "5f8ed5daa93b28103383252f",
            bn: "GENERAL MOTORS",
            pn: "93740275",
            desc: "Бампер передний",
            isOrig: true,
            imagesUlr: ["http://www.google.com"],
            partan: [
                {
                    ptnId: "5f8f32c6e20bd2070fd9fe22",
                    relScore: 100,
                },
            ],
        },
        {
            _id: "5f8ed64aa93b281033832530",
            bn: "GENERAL MOTORS",
            pn: "96542983",
            desc: "Бампер передний",
            isOrig: true,
            imagesUlr: ["http://www.google.com"],
            partan: [
                {
                    ptnId: "5f8f32c6e20bd2070fd9fe22",
                    relScore: 100,
                },
            ],
        },
        {
            _id: "5f8ed667a93b281033832531",
            bn: "GENERAL MOTORS",
            pn: "96481330",
            desc: "Бампер передний",
            isOrig: true,
            imagesUlr: ["http://www.google.com"],
            partan: [
                {
                    ptnId: "5f8f32c6e20bd2070fd9fe22",
                    relScore: 100,
                },
            ],
        },
        {
            _id: "5f8ed6a0a93b281033832532",
            bn: "GENERAL MOTORS",
            pn: "96598571",
            desc: "Бампер задний",
            isOrig: true,
            imagesUlr: ["http://www.google.com"],
            partan: [
                {
                    ptnId: "5f91262946cd8821bedf1a14",
                    relScore: 100,
                },
            ],
        },
        {
            _id: "5f8ed6b2a93b281033832533",
            bn: "GENERAL MOTORS",
            pn: "96543014",
            desc: "Бампер задний",
            isOrig: true,
            imagesUlr: ["http://www.google.com"],
            partan: [
                {
                    ptnId: "5f91262946cd8821bedf1a14",
                    relScore: 100,
                },
            ],
        },
        {
            _id: "5f8ed6c8a93b281033832534",
            bn: "GENERAL MOTORS",
            pn: "96455262",
            desc: "Бампер задний",
            isOrig: true,
            imagesUlr: ["http://www.google.com"],
            partan: [
                {
                    ptnId: "5f91262946cd8821bedf1a14",
                    relScore: 100,
                },
            ],
        },
        {
            _id: "5f8ed6dfa93b281033832535",
            bn: "GENERAL MOTORS",
            pn: "96543017",
            desc: "Бампер задний",
            isOrig: true,
            imagesUlr: ["http://www.google.com"],
            partan: [
                {
                    ptnId: "5f91262946cd8821bedf1a14",
                    relScore: 100,
                },
            ],
        },
        {
            _id: "5f8ed721a93b281033832536",
            bn: "GENERAL MOTORS",
            pn: "96534653",
            desc: "Комплект колодок",
            isOrig: true,
            imagesUlr: ["http://www.google.com"],
            partan: [
                {
                    ptnId: "5f91265246cd8821bedf1a15",
                    relScore: 100,
                },
            ],
        },
        {
            _id: "5f8ed738a93b281033832537",
            bn: "GENERAL MOTORS",
            pn: "96405129",
            desc: "Комплект колодок",
            isOrig: true,
            imagesUlr: ["http://www.google.com"],
            partan: [
                {
                    ptnId: "5f91265246cd8821bedf1a15",
                    relScore: 100,
                },
            ],
        },
        {
            _id: "5f8ed855a93b281033832538",
            bn: "GENERAL MOTORS",
            pn: "93742764",
            desc: "Бампер передний",
            isOrig: true,
            imagesUlr: ["http://www.google.com"],
            partan: [
                {
                    ptnId: "5f91265c46cd8821bedf1a16",
                    relScore: 100,
                },
            ],
        },
        {
            _id: "5f8ed944a93b281033832539",
            bn: "GENERAL MOTORS",
            pn: "96648503",
            desc: "Бампер передний",
            isOrig: true,
            imagesUlr: ["http://www.google.com"],
            partan: [
                {
                    ptnId: "5f91265c46cd8821bedf1a16",
                    relScore: 100,
                },
            ],
        },
        {
            _id: "5f8ed9d8a93b28103383253b",
            bn: "GENERAL MOTORS",
            pn: "96808139",
            desc: "Бампер передний",
            isOrig: true,
            imagesUlr: ["http://www.google.com"],
            partan: [
                {
                    ptnId: "5f91265c46cd8821bedf1a16",
                    relScore: 100,
                },
            ],
        },
        {
            _id: "5f8eda36a93b28103383253c",
            bn: "GENERAL MOTORS",
            pn: "95978888",
            desc: "Бампер задний",
            isOrig: true,
            imagesUlr: ["http://www.google.com"],
            partan: [
                {
                    ptnId: "5f91266a46cd8821bedf1a17",
                    relScore: 100,
                },
            ],
        },
        {
            _id: "5f8eda51a93b28103383253d",
            bn: "GENERAL MOTORS",
            pn: "96648646",
            desc: "Бампер задний",
            isOrig: true,
            imagesUlr: ["http://www.google.com"],
            partan: [
                {
                    ptnId: "5f91266a46cd8821bedf1a17",
                    relScore: 100,
                },
            ],
        },
        {
            _id: "5f8eda70a93b28103383253e",
            bn: "GENERAL MOTORS",
            pn: "95978858",
            desc: "Бампер задний",
            isOrig: true,
            imagesUlr: ["http://www.google.com"],
            partan: [
                {
                    ptnId: "5f91266a46cd8821bedf1a17",
                    relScore: 100,
                },
            ],
        },
        {
            _id: "5f8eda8aa93b28103383253f",
            bn: "GENERAL MOTORS",
            pn: "96648654",
            desc: "Бампер задний",
            isOrig: true,
            imagesUlr: ["http://www.google.com"],
            partan: [
                {
                    ptnId: "5f91266a46cd8821bedf1a17",
                    relScore: 100,
                },
            ],
        },
        {
            _id: "5f8edad5a93b281033832540",
            bn: "GENERAL MOTORS",
            pn: "96808256",
            desc: "Бампер задний",
            isOrig: true,
            imagesUlr: ["http://www.google.com"],
            partan: [
                {
                    ptnId: "5f91266a46cd8821bedf1a17",
                    relScore: 100,
                },
            ],
        },
        {
            _id: "5f8edae9a93b281033832541",
            bn: "GENERAL MOTORS",
            pn: "96808268",
            desc: "Бампер задний",
            isOrig: true,
            imagesUlr: ["http://www.google.com"],
            partan: [
                {
                    ptnId: "5f91266a46cd8821bedf1a17",
                    relScore: 100,
                },
            ],
        },
        {
            _id: "5f8edb2ba93b281033832542",
            bn: "GENERAL MOTORS",
            pn: "94566892",
            desc: "Комплект колодок",
            isOrig: true,
            imagesUlr: ["http://www.google.com"],
            partan: [
                {
                    ptnId: "5f91267446cd8821bedf1a18",
                    relScore: 100,
                },
            ],
        },
    ];
    const switches = [
        false, // makes
        false, // models
        false, // modifs
        false, // partNodeGroups
        true, // partNodes
        true, // genParts
        false, // partans
    ];
    for (let i in switches) {
        console.log(switches[i], i);
        if (!switches[i]) continue;

        let items = makes;
        let indexName = "makes";
        const is = Number(i)
        switch (is) {
            case 0:
                console.log("case 0:");
                items = makes;
                indexName = "makes";
                break;
            case 1:
                console.log("case 1:");
                items = models;
                indexName = "models";
                break;
            case 2:
                console.log("case 2:");
                items = modifs;
                indexName = "modifs";
                break;
            case 3:
                console.log("case 3:");
                items = partNodeGroups;
                indexName = "v3partNodeGroups";
                break;
            case 4:
                console.log("case 4:");
                items = partNodes;
                indexName = "v3partNodes";
                break;
            case 5:
                console.log("case 5:");
                items = genParts;
                indexName = "v3genParts";
                break;
            case 6:
                console.log("case 6:");
                items = partans;
                indexName = "partans";
                break;
            default:
                console.log("def", i)
                break;
        }
        console.log(indexName);
        for (let i in items) {
            const item = items[i];
            insertFunction(item, indexName.toLocaleLowerCase(), "v3genParts");
        }
    }
    // searchFunction("передний бампер");

    // await client.delete({
    //     index: 'cars',
    // type: 'make',
    //     id: '8hIUUXUB5UVcK16gxB4F'
    //   });

    // await client.indices.delete({index: 'partans'})

    // await client.indices.refresh({ index: "cars" });
})();
