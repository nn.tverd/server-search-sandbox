// var elasticsearch = require("elasticsearch");

// const genPart = require("./mongoose/models/genPart");
// const partNode = require("./mongoose/models/partNode");
// const partNodeGroup = require("./mongoose/models/partNodeGroup");

// const carMake = require("./mongoose/models/carMake");
// const carModel = require("./mongoose/models/carModel");
// const carModif = require("./mongoose/models/carModif");

const partan = require("./mongoose/models/partan");
// ===========================================================================
// var client = new elasticsearch.Client({
//     host: "localhost:9200",
//     // log: "trace",
//     apiVersion: "7.2", // use the same version of your Elasticsearch instance
// });

// client.ping(
//     {
//         // ping usually has a 3000ms timeout
//         requestTimeout: 5000,
//     },
//     function (error) {
//         if (error) {
//             console.trace("elasticsearch cluster is down!");
//         } else {
//             // console.log("All is well");
//         }
//     }
// );
// ===========================================================================

function searchFilters(Schema, filters) {
    return new Promise((resolve, reject) => {
        const x = Schema.search(
            {
                ids: {
                    values: filters,
                    boost: 33,
                },
            },
            function (err, results) {
                if (err) {
                    console.log(err);
                    reject(err);
                }
                console.log("IDS: ", results.hits.hits);
                let _gps = results.hits;
                resolve(_gps);
            }
        );
    });
}

function searchSimpleQuery(Schema, queryString) {
    return new Promise((resolve, reject) => {
        const x = Schema.search(
            {
                query_string: {
                    query: queryString,
                    fields: [
                        "positionalTags^1.5",
                        "tags",
                        "units^0.5",
                        "name^8",
                        "year^4",
                    ],
                    // boost: 1,
                },
            },
            function (err, results) {
                if (err) {
                    console.log(err);
                    reject(err);
                }
                console.log(results.hits.hits);
                let _gps = results.hits;
                resolve(_gps);
            }
        );
    });
}

function searchDiagramForPartans(ids) {
    return new Promise((resolve, reject) => {
        const firstIds = ids;

        // -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
        // get
        /*
match (dg:Diagram)-[r4:ND_DG]->(nd:Partnode)<-[r5]-(gp:Genpart)<-[r6]-(ptn:Partan)-[r1]-(mf:Carmodif)-[r2]-(dg2:Diagram)
where dg=dg2
and ptn.m_id in ["5f8f32c6e20bd2070fd9fe22"]
return ptn, dg, apoc.create.vRelationship(ptn, "PT_DG", {score:toFloat(r1.score*r2.score*r4.score*r5.score*r6.score)*0.00000001}, dg) as rel
        */

        const firstIdsStr = JSON.stringify(firstIds);
        let carCypherQuery = `match (dg:Diagram)-[r4:ND_DG]->(nd:Partnode)<-[r5]-(gp:Genpart)<-[r6]-(ptn:Partan)-[r1]-(mf:Carmodif)-[r2]-(dg2:Diagram) `;
        // let carCypherQuery = `match (ptn:Partan)-[r1]->(${meta[0].name})-[r2]->(${meta[1].name})-[r3]->(${meta[2].name}) `;
        carCypherQuery += ` where dg=dg2 `;
        carCypherQuery += ` and ptn.m_id in ${firstIdsStr} `;
        carCypherQuery += ` return ptn, dg, apoc.create.vRelationship(ptn, "PT_DG", {score:toFloat(r1.score*r2.score*r4.score*r5.score*r6.score)*0.00000001}, dg) as rel `;

        // console.log("carCypherQuery: ", carCypherQuery);
        partan
            .cypherQuery(carCypherQuery)
            .then((result) => {
                // console.log(result);
                resolve(result);
            })
            .catch((error) => {
                // console.log(error);
                reject(error);
            });
    });
}

function updateNeoPathes(neoResults, meta) {
    return new Promise((resolve, reject) => {
        const Ids = Object.keys(neoResults.partans);

        const IdsStr = JSON.stringify(Ids);
        let carCypherQuery = `match (ptn:Partan)-[r1:${meta[0].label}]->(${meta[0].name})-[r2:${meta[1].label}]->(${meta[1].name})-[r3:${meta[2].label}]->(${meta[2].name}) `;
        // let carCypherQuery = `match (ptn:Partan)-[r1]->(${meta[0].name})-[r2]->(${meta[1].name})-[r3]->(${meta[2].name}) `;
        carCypherQuery += ` where ptn.m_id in ${IdsStr} `;
        carCypherQuery += ` return ptn, ${meta[0].name}, ${meta[1].name}, ${meta[2].name} , r1, apoc.create.vRelationship(ptn, "R2",{score: toFloat(r1.score*r2.score)*0.01}, ${meta[1].name}) as r2, apoc.create.vRelationship(ptn, "R3",{score: toFloat(r1.score*r2.score*r3.score)*0.0001}, ${meta[2].name}) as r3 `;

        console.log("carCypherQuery: ", carCypherQuery);
        partan
            .cypherQuery(carCypherQuery)
            .then((result) => {
                // console.log(result);
                resolve(result);
            })
            .catch((error) => {
                // console.log(error);
                reject(error);
            });
    });
}

function convirtESHitsToObject(hits, object) {
    for (let i in hits) {
        const hit = hits[i];
        if (!object[hit._index]) {
            object[hit._index] = {};
        }
        if (!object[hit._index][hit._id]) {
            object[hit._index][hit._id] = hit;
        } else {
            object[hit._index][hit._id]._score += hit._score;
        }
    }
}

function extractPartansFromNeo4jRes(neo4jRes1, partansFromNeo, generalResults) {
    console.log(
        "function extractPartansFromNeo4jRes(neo4jRes1, partansFromNeo, generalResults) {",
        generalResults
    );
    const { partans, otherNodes } = partansFromNeo;
    console.log(neo4jRes1.records.length);
    for (let i in neo4jRes1.records) {
        const record = neo4jRes1.records[i];
        const pIndex = 0;
        const n1Index = 1;
        const n2Index = 2;
        const n3Index = 3;
        const r1Index = 4;
        const r2Index = 5;
        const r3Index = 6;
        const partanNode = record._fields[pIndex];
        const n1 = record._fields[n1Index];
        const n2 = record._fields[n2Index];
        const n3 = record._fields[n3Index];
        const _id = partanNode.properties.m_id;
        if (!partans[_id]) {
            partans[_id] = {
                _id: _id,
                score: 0,
            };
        }

        const n1_m_id = n1.properties.m_id;
        const n2_m_id = n2.properties.m_id;
        const n3_m_id = n3.properties.m_id;
        partans[_id].score +=
            Number(record._fields[r1Index].properties.score) * 0.01;
        partans[_id].score +=
            Number(record._fields[r2Index].properties.score) * 0.01;
        partans[_id].score +=
            Number(record._fields[r3Index].properties.score) * 0.01;
        for (let j in generalResults) {
            const grIndex = generalResults[j];
            partans[_id].score += grIndex[n1_m_id]
                ? grIndex[n1_m_id]._score
                : 0;
            partans[_id].score += grIndex[n2_m_id]
                ? grIndex[n2_m_id]._score
                : 0;
            partans[_id].score += grIndex[n3_m_id]
                ? grIndex[n3_m_id]._score
                : 0;
        }
        if (!otherNodes[n1_m_id]) {
            otherNodes[n1_m_id] = {
                _id: n1_m_id,
                score: 0,
            };
        }
        if (!otherNodes[n2_m_id]) {
            otherNodes[n2_m_id] = {
                _id: n2_m_id,
                score: 0,
            };
        }
        if (!otherNodes[n3_m_id]) {
            otherNodes[n3_m_id] = {
                _id: n3_m_id,
                score: 0,
            };
        }
        const ptnIndex = generalResults.partans;
        console.log(ptnIndex);
        let updateScore = 0;
        if (ptnIndex) {
            updateScore = ptnIndex[_id] ? ptnIndex[_id]._score * 0.01 : 0;
        }
        console.log("updateScore", n1_m_id, n2_m_id, n3_m_id, updateScore);
        otherNodes[n1_m_id].score +=
            Number(record._fields[r1Index].properties.score) * 0.01 +
            updateScore;
        otherNodes[n2_m_id].score +=
            Number(record._fields[r2Index].properties.score) * 0.01 +
            updateScore;
        otherNodes[n3_m_id].score +=
            Number(record._fields[r3Index].properties.score) * 0.01 +
            updateScore;
    }
}

function searchElasticWithNeoIds(ids) {
    const query = {
        query: {
            bool: {
                should: [],
            },
        },
    };
    for (let i in ids) {
        query.query.bool.should.push({
            term: {
                _id: {
                    value: ids[i]._id,
                    boost: ids[i].score,
                },
            },
        });
    }
    console.log("query", JSON.stringify(query));
    const carUpdatesResults = client.search({
        index: [
            "modifs",
            "makes",
            "models",
            "v3partnodegroups",
            "v3partnodes",
            "v3genparts",
            "partans",
        ],
        // index: "v3partnodes",
        body: query,
        size: 50,
    });
    return carUpdatesResults;
}

async function partanDiagramsNeoSearch(partansArray) {
    console.log("diagrams for partans neo4j search is started...");
    // -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
    // console.log(partansArray);
    const partanIds = partansArray.partans
        ? Object.keys(partansArray.partans)
        : [];

    const neoRes = await searchDiagramForPartans(partanIds);
    console.log("\n++++++++++++++++++++++++\n", neoRes);
    for (let i in neoRes.records) {
        const record = neoRes.records[i];
        const ptn = record._fields[0];
        const dg = record._fields[1];
        const rel = record._fields[2];
        const ptnId = ptn.properties.m_id;
        const image = {
            url: dg.properties.img,
            score: rel.properties.score,
        };
        if (!partansArray.partans[ptnId].images) {
            partansArray.partans[ptnId].images = [];
        }
        const imageInArray = partansArray.partans[ptnId].images.find((el =>{
            return el.url == image.url;
        }))
        if( !imageInArray )
        partansArray.partans[ptnId].images.push(image);
        else{
            imageInArray.score = Math.max( imageInArray.score, image.score );
        }
    }
    // console.log(  )
    return partansArray;
}

module.exports.partanDiagramsNeoSearch = partanDiagramsNeoSearch;
