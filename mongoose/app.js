const express = require("express");
const mongoose = require("mongoose");
const bodyParser = require("body-parser");
const cors = require("cors");

const app = express();

app.use(cors());

console.log( "\n\n\n\n\n\n\n\n\n\n\n\n" )
console.log( "=======================================================" )// bodyParser = {
console.log( "New start" )// bodyParser = {
console.log( "=======================================================" )// bodyParser = {
console.log( "\n\n" )
//     json: { limit: "50mb", extended: true },
//     urlencoded: { limit: "50mb", extended: true },
// };
// app.use(bodyParser.json());
app.use(bodyParser.json({limit: '50mb', extended: true}))
// app.use(bodyParser.urlencoded({limit: '50mb', extended: true}))



// // // --------------------------------------------------
// const scoresRouters = require("./routers/scores");
// app.use("scores", scoresRouters);
// // // --------------------------------------------------
// const relationsRouters = require("./routers/relations");
// app.use("/relations", relationsRouters);
// // // --------------------------------------------------
// const partanRelRouters = require("./routers/partanRel");
// app.use("/partanrel", partanRelRouters);
// // --------------------------------------------------
const diagramRouters = require("./routers/diagram");
app.use("/diagram", diagramRouters);
// // --------------------------------------------------
const partanRouters = require("./routers/partan");
app.use("/partan", partanRouters);
// // --------------------------------------------------
const partRouters = require("./routers/part");
app.use("/part", partRouters);
// // --------------------------------------------------
const searchPartan = require("./routers/es");
app.use("/es", searchPartan);

// // --------------------------------------------------
const searchCarMake = require("./routers/carMake");
app.use("/carmake", searchCarMake);
// // --------------------------------------------------
const searchCarModel = require("./routers/carModel");
app.use("/carmodel", searchCarModel);
// // --------------------------------------------------
const searchCarModif = require("./routers/carModif");
app.use("/carmodif", searchCarModif);
// // --------------------------------------------------
const searchPartNodeGroup = require("./routers/partNodeGroup");
app.use("/partnodegroup", searchPartNodeGroup);
// // --------------------------------------------------
const searchPartNode = require("./routers/partNode");
app.use("/partnode", searchPartNode);
// // --------------------------------------------------
const searchGenPart = require("./routers/genPart");
app.use("/genpart", searchGenPart);
// // --------------------------------------------------
// const imageRouters = require("./routes/googleImages");
// app.use("/gimage", imageRouters);
// // --------------------------------------------------
// const InputDocsRouters = require("./routes/inputDocs");
// app.use("/inputdocs", InputDocsRouters);
// // --------------------------------------------------
// const FPartRouters = require("./routes/fparts");
// app.use("/fparts", FPartRouters);

mongoose.connect("mongodb://localhost:27017/microapp?retryWrites=false", () => {
    console.log("connected to db...");
});
/**
mongoose.Promise = Promise;
mongoose.set('useCreateIndex', true);
var mongooseOptions = {  useNewUrlParser: true }

mongoose.connect('mongodb://localhost:27017/MyDatabase', mongooseOptions, function(err) {
    if (err) {
        console.error('System could not connect to mongo server.')
        console.log(err)     
    } else {
        console.log('System connected to mongo server.')
    }
});
 */

// middlewares
// app.use("/post", () => {
//     console.log("this is middleware ");
// });

// routes
app.get("/", (req, res) => {
    res.send("we are on home");
});

// how to we start listening to the server
// comment added

app.listen(3033);
