module.exports = {
    syncElastic: true,
    connectElastic: true,
    connectNeo4j: true,
};
