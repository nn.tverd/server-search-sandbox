const mongo4j = require("mongo4j");
console.log("mongo4j is executiong....");
// mongo4j.init('neo4j://localhost', {user: 'neo4j', pass: '123'});
mongo4j.init(
    "bolt://localhost",
    // 'neo4j://localhost',
    { user: "neo4j", pass: "123" },
    {
        encrypted: "ENCRYPTION_OFF",
        maxConnectionLifetime: 3 * 60 * 60 * 1000, // 3 hours
        maxConnectionPoolSize: 150,
        connectionAcquisitionTimeout: 2 * 60 * 1000 * 1000, // 120 seconds * 1000
    }
);

// const mongo4jDriver = mongo4j.getDriver();

// console.log("mongo4jDriver", mongo4jDriver);

// // create session
// const s = mongo4jDriver.session();

// // transaction
// (async () => {
//     console.log("s", s);
//     r = await s.writeTransaction((tx) => f(tx), {
//         timeout: 99999999, // define timeout
//     });
// })();

module.exports = mongo4j;
