const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const mongoosastic = require("mongoosastic");
const ScoreSchema = require("./scores.js");

// const ScoreSchema = new Schema({
//     ScoreT
// });

mongoose.set("debug", false);

// ********************************************************************
console.log("-:ScoreSchema:-", ScoreSchema);
// ********************************************************************

const RelationSchema = Schema(
    {
        aType: { type: String, required: true, es_indexed: true },
        bType: { type: String, required: true, es_indexed: true },
        aId: { type: String, required: true, es_indexed: true },
        bId: { type: String, required: true, es_indexed: true },

        score: { type: Number, required: true, es_indexed: true },
        scores: [
            {
                type: mongoose.Types.ObjectId,
                ref: "score",
                // es_schema: ScoreSchema,
                // es_type: "nested",
                // es_include_in_parent: true,
                es_indexed: true,
                es_select: "type score",
            },
        ],
    },
    { collection: "relations" }
);

RelationSchema.plugin(mongoosastic, {
    index: "relations",
    hosts: ["localhost:9200"],
    hydrate: true,
    hydrateOptions: { lean: true },
    populate: [{ path: "scores", select: 'type score' }],
    // populate: [{ path: "scores", select: "type score" }],
});

// module.exports.RelationSchema = RelationSchema;
module.exports = mongoose.model("relation", RelationSchema);

// _id	name	partNumber	CarMakeSections	catalogs	analogsGroup
