const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const mongoosastic = require("mongoosastic");
require("./relations.js");
// ****************************************************
// ****************************************************

mongoose.set("debug", false);

// const NestedItemSchema = Schema({
//     id: { type: String },
//     relScore: { type: Number, es_type: "integer" },
// });

const PartanSchema = Schema(
    {
        desc: { type: String, es_indexed: true },
        metaDesc: { type: String, es_indexed: true },
        mk: [
            {
                type: mongoose.Types.ObjectId,
                ref: "relation",
                es_indexed: true,
                // es_select: "aId bId score",
                // es_type: "nested",
                // es_include_in_parent: true,
                // es_schema: RelationSchema,
            },
        ],
        md: [
            {
                type: mongoose.Types.ObjectId,
                ref: "relation",
                es_indexed: true,
                // es_select: "aId bId score",
                // es_type: "nested",
                // es_include_in_parent: true,
                // es_schema: RelationSchema,
            },
        ],
        mf: [
            {
                type: mongoose.Types.ObjectId,
                ref: "relation",
                es_indexed: true,
                // es_select: "aId bId score",
                // es_type: "nested",
                // es_include_in_parent: true,
                // es_schema: RelationSchema,
            },
        ],
        gn: [
            {
                type: mongoose.Types.ObjectId,
                ref: "relation",
                es_indexed: true,
                // es_select: "aId bId score",
                // es_type: "nested",
                // es_include_in_parent: true,
                // es_schema: RelationSchema,
            },
        ],
        nd: [
            {
                type: mongoose.Types.ObjectId,
                ref: "relation",
                es_indexed: true,
                // es_select: "aId bId score",
                // es_type: "nested",
                // es_include_in_parent: true,
                // es_schema: RelationSchema,
            },
        ],
        gp: [
            {
                type: mongoose.Types.ObjectId,
                ref: "relation",
                es_indexed: true,
                // es_select: "aId bId score",
                // es_type: "nested",
                // es_include_in_parent: true,
                // es_schema: RelationSchema,
            },
        ],
    },
    { collection: "partanRel" }
);

PartanSchema.plugin(mongoosastic, {
    index: "partanrels",
    hosts: ["localhost:9200"],
    hydrate: true,
    hydrateOptions: { lean: true },
    populate: [
        { path: "mk", select: "aId bId score" },
        { path: "md", select: "aId bId score" },
        { path: "mf", select: "aId bId score" },
        { path: "gp", select: "aId bId score" },
        { path: "gn", select: "aId bId score" },
        { path: "nd", select: "aId bId score" },
    ],
});

module.exports = mongoose.model("partanRel", PartanSchema);

// _id	name	partNumber	partSections	catalogs	analogsGroup
