const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const mongoosastic = require("mongoosastic");

mongoose.set("debug", false);

const ScoreSchema = Schema(
    {
        relId: { type: String, required: true, es_indexed: true },
        author: { type: String, required: true, es_indexed: true },
        type: { type: String, required: true, es_indexed: true },
        description: { type: String },

        score: { type: Number, required: true, es_indexed: true }, // must be 0 - 1 float
    },
    { collection: "scores" }
);

ScoreSchema.plugin(mongoosastic, {
    index: "scores",
    hosts: ["localhost:9200"],
    hydrate: true,
    hydrateOptions: { lean: true },
});

// module.exports.ScoreSchema = ScoreSchema;
module.exports= mongoose.model("score", ScoreSchema);

// _id	name	partNumber	CarMakeSections	catalogs	analogsGroup
