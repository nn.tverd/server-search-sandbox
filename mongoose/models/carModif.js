const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const mongoosastic = require("mongoosastic");
const mongo4j = require("./plugins/mongo4j");
const { connectElastic, connectNeo4j } = require("./plugins/config");

// mongoose.set("debug", false);

const Diagram = Schema({
    img: { type: String },
    importedNodeName: { type: String },
    nodeGroupId: { type: String },
    nodeId: { type: String },
    relScore: { type: Number },
});

const CarModifSchema = Schema(
    {
        name: {
            type: String,
            required: true,
            es_indexed: true,
            neo_prop: true,
        },
        year: { type: String, es_indexed: true, neo_prop: true },
        syns: {
            type: Array,
            es_indexed: true,
            // es_boost: 1.0,
            es_type: "string",
        },
        // makeId: {
        //     type: String,
        //     es_indexed: true,
        // },
        mds: {
            _id: {
                type: Schema.Types.ObjectId,
                ref: "carModel",
                es_indexed: true,
                neo_rel_name: "MD_MF",
            },
            score: Number,
        },
        tags: {
            type: Array,
            es_indexed: true,
            // es_analyzer: "rebuilt_russian",
            // es_boost: 1.0,
            es_type: "string",
        },
        meta: {
            type: Object,
        },
    },
    { collection: "carModif" }
);

if (connectElastic) {
    CarModifSchema.plugin(mongoosastic, {
        index: "modifs",
        hosts: ["localhost:9200"],
        // hydrate: true,
        // hydrateOptions: { lean: true },
    });
}
if (connectNeo4j) {
    CarModifSchema.plugin(mongo4j.plugin());
}

module.exports = mongoose.model("carModif", CarModifSchema);

// _id	name	partNumber	CarModifSections	catalogs	analogsGroup
