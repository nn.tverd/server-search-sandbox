const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const mongoosastic = require("mongoosastic");
const mongo4j = require("./plugins/mongo4j");
const { connectElastic, connectNeo4j } = require("./plugins/config");

// mongoose.set("debug", false);

const PartNodeSchema = Schema(
    {
        name: {
            type: String,
            required: true,
            es_indexed: true,
            es_analyzer: "rebuilt_russian",
            // es_boost: 3.0,
            neo_prop: true,
        },
        units: {
            type: Array,
            es_indexed: true,
            es_analyzer: "rebuilt_russian",
            // es_boost: 0.5,
            es_type: "string",
        },
        nameDisplay: {
            type: String,
        },
        positionalTags: {
            type: Array,
            es_indexed: true,
            es_analyzer: "rebuilt_russian",
            // es_boost: 2.0,
            es_type: "string",
            // neo_prop: true,
        },
        tags: {
            type: Array,
            es_indexed: true,
            es_analyzer: "rebuilt_russian",
            // es_boost: 1.0,
            es_type: "string",
        },
        gns: {
            _id: {
                type: Schema.Types.ObjectId,
                ref: "partNodeGroup",
                es_indexed: true,
                neo_rel_name: "GN_ND",
            },
            score: Number,
        },
        meta: {
            type: Object,
        },
    },
    { collection: "partNode" }
);
if (connectElastic) {
    PartNodeSchema.plugin(mongoosastic, {
        index: "v3partnodes",
        hosts: ["localhost:9200"],
        // hydrate: true,
        // hydrateOptions: { lean: true },
    });
}
if (connectNeo4j) {
    PartNodeSchema.plugin(mongo4j.plugin());
}

module.exports = mongoose.model("partNode", PartNodeSchema);

// _id	name	partNumber	CarMakeSections	catalogs	analogsGroup
