const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const mongoosastic = require("mongoosastic");
const mongo4j = require("./plugins/mongo4j");
const { connectElastic, connectNeo4j } = require("./plugins/config");

// mongoose.set("debug", true);

const GenPartSchema = Schema(
    {
        name: {
            type: String,
            required: true,
            es_indexed: true,
            // es_boost: 3.0,
            es_analyzer: "rebuilt_russian",
            neo_prop: true,
        },
        units: {
            type: Array,
            es_indexed: true,
            es_analyzer: "rebuilt_russian",
            // es_boost: 0.5,
            es_type: "string",
        },
        nameDisplay: {
            type: String,
        },
        positionalTags: {
            type: Array,
            es_indexed: true,
            es_analyzer: "rebuilt_russian",
            // es_boost: 0.01,
            es_type: "string",
            // neo_prop: true,
        },
        tags: {
            type: Array,
            es_indexed: true,
            es_analyzer: "rebuilt_russian",
            // es_boost: 1.0,
            es_type: "string",
        },
        nds: {
            _id: {
                type: Schema.Types.ObjectId,
                ref: "partNode",
                es_indexed: true,
                neo_rel_name: "ND_GP",
            },
            score: Number,
        },
        meta: {
            type: Object,
        },
    },
    { collection: "genPart" }
);

if (connectNeo4j) {
    GenPartSchema.plugin(mongo4j.plugin());
}

if (connectElastic) {
    GenPartSchema.plugin(mongoosastic, {
        index: "v3genparts",
        hosts: ["localhost:9200"],
        // hydrate: true,
        // hydrateOptions: { lean: true },
    });
}

module.exports = mongoose.model("genPart", GenPartSchema);

// _id	name	partNumber	CarMakeSections	catalogs	analogsGroup
