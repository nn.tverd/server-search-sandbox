const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const mongoosastic = require("mongoosastic");
const mongo4j = require("./plugins/mongo4j");

const { connectElastic, connectNeo4j } = require("./plugins/config");

mongoose.set("debug", false);

const CarMakeSchema = Schema(
    {
        name: {
            type: String,
            required: true,
            es_indexed: true,
            neo_prop: true,
        },
        syns: {
            type: Array,
            es_indexed: true,
            // es_boost: 1.0,
            es_type: "string",
        },
        tags: {
            type: Array,
            es_indexed: true,
            // es_analyzer: "rebuilt_russian",
            // es_boost: 1.0,
            es_type: "string",
        },
        meta: {
            type: Object,
        },
    },
    { collection: "carMake" }
);
if (connectElastic) {
    CarMakeSchema.plugin(mongoosastic, {
        index: "makes",
        hosts: ["localhost:9200"],
        // hydrate: true,
        // hydrateOptions: { lean: true },
    });
}
if (connectNeo4j) {
    CarMakeSchema.plugin(mongo4j.plugin());
}

module.exports = mongoose.model("carMake", CarMakeSchema);

// _id	name	partNumber	CarMakeSections	catalogs	analogsGroup
