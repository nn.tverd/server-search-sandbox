const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const mongoosastic = require("mongoosastic");
// const { schema } = require("./partNode");
const mongo4j = require("./plugins/mongo4j");
// mongoose.set("debug", true);
const { connectElastic, connectNeo4j } = require("./plugins/config");

// const NestedItemSchema = Schema({
//     id: { type: Schema.Types.ObjectId },
//     relScore: { type: Number, es_type: "long" },
// });

const PartanSchema = Schema(
    {
        desc: { type: String, es_indexed: true, neo_prop: true },
        metaDesc: { type: String, es_indexed: true },
        mf: [
            {
                _id: {
                    type: Schema.Types.ObjectId,
                    ref: "carModif",
                    es_indexed: true,
                    neo_rel_name: "MF_PT",
                },
                score: Number,
            },
        ],

        gp: [
            {
                _id: {
                    type: Schema.Types.ObjectId,
                    ref: "genPart",
                    es_indexed: true,
                    neo_rel_name: "GP_PT",
                },
                score: Number,
            },
        ],
        meta: {
            type: Object,
        },
        isActive: {
            type: Boolean,
            required: true,
            default: true,
            es_indexed: true,
        },
    },
    { collection: "partan" }
);
if (connectElastic) {
    PartanSchema.plugin(mongoosastic, {
        index: "partans",
        hosts: ["localhost:9200"],
        // hydrate: true,
        // hydrateOptions: { lean: true },
    });
}
if (connectNeo4j) {
    PartanSchema.plugin(mongo4j.plugin());
}

module.exports = mongoose.model("partan", PartanSchema);

// _id	name	partNumber	partSections	catalogs	analogsGroup
