const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const mongoosastic = require("mongoosastic");
const mongo4j = require("./plugins/mongo4j");
const { connectElastic, connectNeo4j } = require("./plugins/config");
// const mongo4j = require("mongo4j");

// mongoose.set("debug", false);
// mongo4j.init("neo4j://localhost", { user: "neo4j", pass: "123" });

const CarModelSchema = Schema(
    {
        name: {
            type: String,
            required: true,
            es_indexed: true,
            neo_prop: true,
        },
        syns: {
            type: Array,
            es_indexed: true,
            // es_boost: 1.0,
            es_type: "string",
        },
        mks: {
            _id: {
                type: Schema.Types.ObjectId,
                ref: "carMake",
                es_indexed: true,
                neo_rel_name: "MK_MD",
            },
            score: Number,
        },
        tags: {
            type: Array,
            es_indexed: true,
            // es_analyzer: "rebuilt_russian",
            // es_boost: 1.0,
            es_type: "string",
        },
        yearFrom: {
            type: Number,
            es_indexed: true,
            // es_analyzer: "rebuilt_russian",
            // es_boost: 1.0,
            // es_type: "string",
        },
        yearTo: {
            type: Number,
            es_indexed: true,
            // es_analyzer: "rebuilt_russian",
            // es_boost: 1.0,
            // es_type: "string",
        },
        meta: {
            type: Object,
        },
    },
    { collection: "carModel" }
);
if (connectElastic) {
    CarModelSchema.plugin(mongoosastic, {
        index: "models",
        hosts: ["localhost:9200"],
        // hydrate: true,
        // hydrateOptions: { lean: true },
    });
}
if (connectNeo4j) {
    CarModelSchema.plugin(mongo4j.plugin());
}

module.exports = mongoose.model("carModel", CarModelSchema);

// _id	name	partNumber	CarModelSections	catalogs	analogsGroup
