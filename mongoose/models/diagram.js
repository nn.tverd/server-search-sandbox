const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const mongoosastic = require("mongoosastic");
const mongo4j = require("./plugins/mongo4j");
const { connectElastic, connectNeo4j } = require("./plugins/config");

// mongoose.set("debug", false);

const DiagramSchema = Schema(
    {
        img: {
            type: String,
            required: true,
            es_indexed: true,
            neo_prop: true,
        },
        mf: [
            {
                _id: {
                    type: Schema.Types.ObjectId,
                    ref: "carModif",
                    es_indexed: true,
                    neo_rel_name: "MF_DG",
                },
                score: Number,
            },
        ],
        nd: [
            {
                _id: {
                    type: Schema.Types.ObjectId,
                    ref: "partNode",
                    es_indexed: true,
                    neo_rel_name: "ND_DG",
                },
                score: Number,
            },
        ],
        meta: {
            type: Object,
        },
    },
    { collection: "diagram" }
);
if (connectElastic) {
    DiagramSchema.plugin(mongoosastic, {
        index: "diagrams",
        hosts: ["localhost:9200"],
        // hydrate: true,
        // hydrateOptions: { lean: true },
    });
}
if (connectNeo4j) {
    DiagramSchema.plugin(mongo4j.plugin());
}

module.exports = mongoose.model("diagram", DiagramSchema);

// _id	name	partNumber	CarMakeSections	catalogs	analogsGroup
