const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const mongoosastic = require("mongoosastic");
const mongo4j = require("./plugins/mongo4j");
const { connectElastic, connectNeo4j } = require("./plugins/config");

// mongoose.set("debug", false);

const PartNodeGroupSchema = Schema(
    {
        name: {
            type: String,
            required: true,
            es_indexed: true,
            es_analyzer: "rebuilt_russian",
            // es_boost: 113.0,
            neo_prop: true,
        },
        units: {
            type: Array,
            es_indexed: true,
            es_analyzer: "rebuilt_russian",
            // es_boost: 0.5,
            es_type: "string",
        },
        nameDisplay: {
            type: String,
        },
        positionalTags: {
            type: Array,
            es_indexed: true,
            es_analyzer: "rebuilt_russian",
            // es_boost: 2.0,
            es_type: "string",
            // neo_prop: true,
        },
        tags: {
            type: Array,
            es_indexed: true,
            es_analyzer: "rebuilt_russian",
            // es_boost: 1.0,
            es_type: "string",
        },
        meta: {
            type: Object,
        },
    },
    { collection: "partNodeGroup" }
);
if (connectElastic) {
    PartNodeGroupSchema.plugin(mongoosastic, {
        index: "v3partnodegroups",
        hosts: ["localhost:9200"],
        // hydrate: true,
        // hydrateOptions: { lean: true },
    });
}
if (connectNeo4j) {
    PartNodeGroupSchema.plugin(mongo4j.plugin());
}

module.exports = mongoose.model("partNodeGroup", PartNodeGroupSchema);

// _id	name	partNumber	CarMakeSections	catalogs	analogsGroup
