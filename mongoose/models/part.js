const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const mongoosastic = require("mongoosastic");

// mongoose.set("debug", true);

const PartSchema = Schema(
    {
        bn: { type: String, required: true, es_indexed: true },
        pn: { type: String, required: true, es_indexed: true },
        desc: { type: String },
        _test: { type: String },

        meta: { type: Object },
        partan: [
            {
                _id: {
                    type: Schema.Types.ObjectId,
                    ref: "partan",
                    
                },
                relScore: Number,
            },
        ],
    },
    { collection: "part" }
);

PartSchema.plugin(mongoosastic, {
    index: "parts",
    hosts: ["localhost:9200"],
    hydrate: true,
    hydrateOptions: { lean: true },
});

module.exports = mongoose.model("part", PartSchema);

// _id	name	partNumber	partSections	catalogs	analogsGroup
