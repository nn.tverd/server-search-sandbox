const express = require("express");
const router = express.Router();
const CarModif = require("../models/carModif");
const { syncElastic, connectNeo4j } = require("../models/plugins/config");

// **********************************************************************************************
if (syncElastic) {
    CarModif.createMapping(function (err, mapping) {
        console.log("\n\n\n\n modifs ");
        if (err) {
            console.log("error creating mapping!");
            console.log(err);
        } else {
            console.log("mapping is created!");
            // console.log(mapping);
        }
    });

    const stream = CarModif.synchronize();
    let count = 0;

    stream.on("data", function () {
        count++;
    });
    stream.on("close", function () {
        console.log(`indexed ${count} documnets`);
    });
    stream.on("error", function (err) {
        console.log(err);
    });
}

// **********************************************************************************************

router.get("/", async (req, res) => {
    console.log("get CarModif");
    try {
        const parts = await CarModif.find();
        res.json(parts);
    } catch (err) {
        res.json({ message: err });
    }
});
// **********************************************************************************************

router.delete("/many", async (req, res) => {
    console.log("get CarModif");
    try {
        const parts = await CarModif.deleteMany({});
        res.json(parts);
    } catch (err) {
        res.json({ message: err });
    }
});
module.exports = router;
