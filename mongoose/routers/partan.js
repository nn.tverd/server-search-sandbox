const express = require("express");
const router = express.Router();
const Partan = require("../models/partan");
const Part = require("../models/part");
const mongoose = require("mongoose");
const { syncElastic, connectNeo4j } = require("../models/plugins/config");

const SchemaType = mongoose.Types;

const helperCombineRefs = (arrayMain, arrayCandidate) => {
    for (let i in arrayCandidate) {
        const _mf = arrayCandidate[i];
        const findResult = arrayMain.find((a) => {
            return a._id === _mf._id;
        });
        if (findResult) {
            findResult.score = Math.max(findResult.score, _mf.score);
        } else {
            arrayMain.push(_mf);
        }
    }
};

// **********************************************************************************************
if (syncElastic) {
    Partan.createMapping({}, function (err, mapping) {
        if (err) {
            console.log("\n\n\n\n PARTAN ");
            console.log("error creating mapping!");
            console.log(err);
        } else {
            console.log("mapping is created!");
            // console.log(mapping);
        }
    });

    const stream = Partan.synchronize();
    let count = 0;

    stream.on("data", function () {
        // console.log(`on.data indexed ${count} documnets`);
        count++;
    });
    stream.on("close", function () {
        console.log(`indexed ${count} documnets`);
    });
    stream.on("error", function (err) {
        console.log(err);
    });
}

// **********************************************************************************************
// **********************************************************************************************

router.get("/", async (req, res) => {
    console.log("get Partan");
    try {
        const partans = await Partan.find();
        res.json(partans);
    } catch (err) {
        res.json({ message: err });
    }
});

// **********************************************************************************************

router.post("/search", async (req, res) => {
    const b = req.body;
    const cond = b.cond;
    cond["mk.mkId"]["$in"].forEach((item) => {
        console.log(item);
        item = mongoose.Types.ObjectId(item);
        console.log(item);
        return (item = mongoose.Types.ObjectId(item));
    });
    const scores = b.scores;
    console.log(req.body);
    try {
        let partans = [];
        partans = await Partan.find(cond);
        partans.forEach((item) => {
            return (item.initScore = scores[item.mk.mkId]);
        });
        console.log(partans);
        res.json(partans);
    } catch (err) {
        res.json({ message: err });
    }
});

// **********************************************************************************************

router.post("/", async (req, res) => {
    console.log("router.post(/, async (req, res) => {", req.body);
    const b = req.body;
    const partan = new Partan({
        dataBaseId: b.dataBaseId,
        name: b.name,
        partanNumber: b.partanNumber,
        partanSections: b.partanSections,
        catalogs: b.catalogs,
        analogsGroup: b.analogsGroup,
    });

    try {
        const savedPartan = await partan.save();
        res.json(savedPartan);
    } catch (err) {
        res.json({ message: err });
    }
});

// **********************************************************************************************

router.post("/insertPartan", async (req, res) => {
    // {
    //   constSec:{ bn: 1, pn:2, }
    //   payloadSec:{}
    // }
    const b = req.body;

    console.log("insertPartan");
    const objectList = b;
    // for (let i in objectList) {
    // const cs = b.constSec;
    // const ps = b.payloadSec;
    const newObject = objectList;
    if (!newObject.payload || !newObject.payload.brandName) {
        // mark it as empry and skip insertion
    } else {
        const bn = newObject.payload.brandName.toUpperCase();
        const pn = newObject.payload.partanNumber
            .replace(/[^a-zA-Zа-яА-Я0-9 ]/g, "")
            .toUpperCase();
        const name = newObject.payload.name;

        const upsertObj = {
            _id: {
                bn: bn,
                pn: pn,
            },
            bn: bn,
            pn: pn,
            pubNam: name,
        };
        console.log(newObject._id);
        // console.log(newObject);
        try {
            const upsRes = await Partan.updateOne(
                { _id: upsertObj._id },
                { $set: { ...upsertObj } },
                { upsert: true }
            );
            console.log(
                "=============================================\n",
                upsRes,
                "\n=============================================",
                "\n"
            );
            // const foundObject = await Partan.findOne({
            //     _id: upsertObj._id,
            // });
            // console.log(foundObject);

            // const pld = foundObject.paylaod ? foundObject.paylaod : {};
            // pld[newObject._id] = { ...newObject };
            // console.log(pld);
            // const insertPayload = await Partan.updateOne(
            //     { _id: upsertObj._id },
            //     { ...foundObject }
            // );
            // console.log(pld);

            // console.log("payload updated", insertPayload);
        } catch (err) {
            console.log(err);
            res.json({ message: err });
        }
        // }
    }
    console.log({ message: "update is ok" });

    res.json({ message: "update is ok" });
});

// **********************************************************************************************

// adding crosses from armtek without tecknical data
router.post("/gettotreat", async (req, res) => {
    const b = req.body;
    const cond = b.cond;
    const limit = b.limit;
    let lock = b.lock;
    if (!lock) lock = 10 * 60000;
    // lock = Number(req.params.count);
    // const count = Number(req.params.count);
    // console.log(count);
    try {
        const foundDocs = await Partan.find({
            ...cond,
            isUnderTreatment: { $lte: new Date().getTime() },
        }).limit(limit);
        if (!foundDocs.length) {
            res.json([]);
            return [];
        }
        const ids = foundDocs.map((item) => {
            return item._id;
        });
        await Partan.updateMany(
            { _id: { $in: ids } },
            { $set: { isUnderTreatment: new Date().getTime() + lock } }
        );
        console.log(foundDocs.length);
        res.json(foundDocs);
    } catch (err) {
        console.log(err);
        res.json({ message: err });
    }
});

// **********************************************************************************************

router.delete("/:partanId", async (req, res) => {
    try {
        const removedPartan = await Partan.remove({ _id: req.params.partanId });

        res.json(removedPartan);
    } catch (err) {
        res.json({ message: err });
    }
});

// **********************************************************************************************

router.patch("/:partanId", async (req, res) => {
    console.log(req.body);
    console.log(req.params.partanId);
    try {
        const updatedPartan = await Partan.updateOne(
            { _id: req.params.partanId },
            { $set: { ...req.body } }
        );

        console.log("updatedPartan", updatedPartan);
        res.json(updatedPartan);
    } catch (err) {
        res.json({ message: err });
    }
});

// **********************************************************************************************

// adding crosses from armtek without tecknical data
router.post("/concat", async (req, res) => {
    console.log("router.post(/concat, async (req, res) => {");
    const b = req.body;
    const { mainId, candidateId } = b;
    console.log(mainId, candidateId);

    const transactionOptions = {
        readPreference: "primary",
        readConcern: { level: "local" },
        writeConcern: { w: "majority" },
    };

    try {
        const session = await mongoose.startSession();
        // const session = await Partan.startSession();
        // The `withTransaction()` function's first parameter is a function
        // that returns a promise.
        await session.withTransaction(async () => {
            const main = await Partan.findOne(
                { _id: SchemaType.ObjectId(mainId) },
                null,
                { session: session }
            );
            const candidate = await Partan.findOne(
                { _id: SchemaType.ObjectId(candidateId) },
                null,
                { session: session }
            );

            if (main._id != mainId) {
                console.log("main._id != mainId");
                return;
            }
            if (candidate._id != candidateId) {
                console.log("candidate._id != candidateId");
                return;
            }
            if (!main.isActive) {
                console.log("!main.isActive");
                return;
            }
            // if (!candidate.isActive) {
            //     console.log("!candidate.isActive");
            //     return;
            // }
            console.log("concat transaction can be done");
            candidate.isActive = false;

            const mf1 = main.mf;
            const mf2 = candidate.mf;
            helperCombineRefs(mf1, mf2);

            const gp1 = main.gp;
            const gp2 = candidate.gp;
            helperCombineRefs(gp1, gp2);

            console.log("still working 1 .....");

            const parts = await Part.find(
                { "partan._id": SchemaType.ObjectId(candidateId) },
                null,
                { session: session }
            );
            console.log(
                "still working 1-1 .....",
                "parts.length",
                parts.length
            );

            for (let i in parts) {
                console.log("still working 1-2 .....");

                const part = parts[i];
                let x = {};
                while (x != undefined) {
                    console.log("still working 1-3 .....");

                    x = part.partan.find((p, index) => {
                        console.log("still working 1-4 .....", index);
                        console.log(
                            "still working 1-4-2 .....",
                            mainId,
                            p._id,
                            candidateId
                        );

                        if (mainId != p._id) return p._id == candidateId;
                        else {
                            return false;
                        }
                    });
                    console.log("still working 1-5 .....", x);

                    if (x != undefined) {
                        x._id = SchemaType.ObjectId(mainId);
                        // console.log( "x has been updated to ", x._id, part )
                    }
                    console.log("still working 1-6 .....", "afterUpdate", x);
                }
            }
            try {
                console.log("still working 2 .....");
                const proms = [];
                proms.push(await candidate.save({ session: session }));
                console.log("still working 2-1 .....", main);
                proms.push(await main.save({ session: session }));
                console.log("still working 2-2 .....");
                for (let p in parts) {
                    console.log("still working 2-3 .....");

                    const part = parts[p];
                    // part._test = "123 - concat";
                    proms.push(await part.save({ session: session }));
                    console.log("still working 2-4 .....");
                }
                // const save3 = parts.save({ session: session });
                console.log("still working 3 .....");
                // const _x = await Promise.all(proms);
                console.log("still working 4 .....");

                // console.log(x);
                return 1;
            } catch (err) {
                console.log(err);
                await session.abortTransaction();
                return null;
            }
        }, transactionOptions);
        const count = await Partan.countDocuments();
        // assert.strictEqual(count, 1);

        session.endSession();
        console.log("finish !");
        res.json({ ok: 1 });
    } catch (err) {
        console.log(err);
        res.json({ message: err });
    }
});

// **********************************************************************************************

// adding crosses from armtek without tecknical data
router.post("/concat2", async (req, res) => {
    console.log("router.post(/concat, async (req, res) => {");
    const b = req.body;
    const { mainId, candidateId } = b;
    console.log(mainId, candidateId);

    const transactionOptions = {
        readPreference: "primary",
        readConcern: { level: "local" },
        writeConcern: { w: "majority" },
    };

    try {
        const session = await mongoose.startSession();
        // const session = await Partan.startSession();
        // The `withTransaction()` function's first parameter is a function
        // that returns a promise.
        await session.withTransaction(async () => {
            const main = await Partan.findOne(
                { _id: SchemaType.ObjectId(mainId) },
                null,
                { session: session }
            );
            const candidate = await Partan.findOne(
                { _id: SchemaType.ObjectId(candidateId) },
                null,
                { session: session }
            );

            if (main._id != mainId) {
                console.log("main._id != mainId");
                return;
            }
            if (candidate._id != candidateId) {
                console.log("candidate._id != candidateId");
                return;
            }
            if (!main.isActive) {
                console.log("!main.isActive");
                return;
            }
            // if (!candidate.isActive) {
            //     console.log("!candidate.isActive");
            //     return;
            // }
            console.log("concat transaction can be done");

            await Partan.updateOne(
                { _id: SchemaType.ObjectId(mainId) },
                {
                    $addToSet: {
                        gp: { $each: candidate.gp },
                        mf: { $each: candidate.mf },
                    },
                },
                { session: session }
            );
            await Partan.updateOne(
                { _id: SchemaType.ObjectId(candidateId) },
                { isActive: false },
                { session: session }
            );
            await Part.updateMany(
                {
                    partan: {
                        $elemMatch: { _id: SchemaType.ObjectId(candidateId) },
                    },
                },
                { $set: { "partan.$._id": mainId } },
                { session: session }
            );
        }, transactionOptions);
        const count = await Partan.countDocuments();
        // assert.strictEqual(count, 1);

        session.endSession();
        console.log("finish !");
        res.json({ ok: 1 });
    } catch (err) {
        console.log(err);
        res.json({ message: err });
    }
});

module.exports = router;
