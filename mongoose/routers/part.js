const express = require("express");
const router = express.Router();
const Part = require("../models/part");
const mongoose = require("mongoose");

// **********************************************************************************************

router.get("/", async (req, res) => {
    console.log("get Part");
    try {
        const parts = await Part.find();
        console.log(parts.length);
        res.json(parts);
    } catch (err) {
        res.json({ message: err });
    }
});

// **********************************************************************************************

router.get("/:partanId", async (req, res) => {
    console.log(req.body);
    console.log(req.params.partanId);
    const { partanId } = req.params;
    try {
        const Parts = await Part.find({
            "partan._id": mongoose.Types.ObjectId(partanId),
        });

        console.log("Parts", Parts);
        res.json(Parts);
    } catch (err) {
        res.json({ message: err });
    }
});
// **********************************************************************************************

router.post("/clearpartan/:partId", async (req, res) => {
    console.log(req.body);
    console.log(req.params);
    const { partId } = req.params;
    const { newPartanArray } = req.body;
    if (!newPartanArray) {
        return res.json({ message: "if( !newPartanArray ){" });
    }
    console.log("clearpartan  1 ......", partId.length);
    try {
        const Parts = await Part.updateOne(
            {
                _id: mongoose.Types.ObjectId(partId),
            },
            {
                $set: {
                    partan: newPartanArray,
                },
            }
        );
        console.log("clearpartan  2 ......");

        console.log("Parts", Parts);
        res.json(Parts);
    } catch (err) {
        console.log("clearpartan  3 ......", err);

        res.json({ message: err });
    }
});

module.exports = router;
