const express = require("express");
const router = express.Router();
const CarMake = require("../models/carMake");

const {
    importCarsFromChevroletDB,
} = require("../../imports/importCarsFromChevroletDB");
const { syncElastic, connectNeo4j } = require("../models/plugins/config");

// **********************************************************************************************
if (syncElastic) {
    CarMake.createMapping(function (err, mapping) {
        console.log("\n\n\n\n CarMake ");
        if (err) {
            console.log("error creating mapping!");
            console.log(err);
        } else {
            console.log("mapping is created!");
            // console.log(mapping);
        }
    });

    const stream = CarMake.synchronize();
    let count = 0;

    stream.on("data", function () {
        count++;
    });
    stream.on("close", function () {
        console.log(`indexed ${count} documnets`);
    });
    stream.on("error", function (err) {
        console.log(err);
    });
}

// **********************************************************************************************

router.get("/", async (req, res) => {
    console.log("get CarMake");
    try {
        const parts = await CarMake.find();
        res.json(parts);
    } catch (err) {
        res.json({ message: err });
    }
});
//importCarsFromChevroletDB
// **********************************************************************************************

router.get("/importcarsfromchevroletdb", async (req, res) => {
    console.log("get CarMake");
    try {
        const results = await importCarsFromChevroletDB();
        res.json(results);
    } catch (err) {
        res.json({ message: err });
    }
});

// **********************************************************************************************

router.get("/", async (req, res) => {
    console.log("get CarMake");
    try {
        const parts = await CarMake.find();
        res.json(parts);
    } catch (err) {
        res.json({ message: err });
    }
});
module.exports = router;
