const express = require("express");
const router = express.Router();
const PartNodeGroup = require("../models/partNodeGroup");
const { syncElastic, connectNeo4j } = require("../models/plugins/config");

// **********************************************************************************************
if (syncElastic) {
    PartNodeGroup.createMapping(
        {
            analysis: {
                filter: {
                    russian_stop: { type: "stop", stopwords: "_russian_" },
                    russian_keywords: {
                        type: "keyword_marker",
                        keywords: ["примера"],
                    },
                    russian_stemmer: {
                        type: "stemmer",
                        language: "russian",
                    },
                    custom_stem: {
                        type: "stemmer_override",
                        rules_path: "analysis/rules.txt",
                    },
                },
                analyzer: {
                    rebuilt_russian: {
                        tokenizer: "standard",
                        filter: [
                            "lowercase",
                            "russian_stop",
                            "russian_keywords",
                            "russian_stemmer",
                            "custom_stem",
                        ],
                    },
                },
            },
        },
        function (err, mapping) {
            console.log("\n\n\n\n PartNodeGroup ");
            if (err) {
                console.log("error creating mapping!");
                console.log(err);
            } else {
                console.log("mapping is created!");
                // console.log(mapping);
            }
        }
    );

    const stream = PartNodeGroup.synchronize();
    let count = 0;

    stream.on("data", function () {
        count++;
    });
    stream.on("close", function () {
        console.log(`indexed ${count} documnets`);
    });
    stream.on("error", function (err) {
        console.log(err);
    });
}

// **********************************************************************************************

router.get("/", async (req, res) => {
    console.log("get PartNodeGroup");
    try {
        const parts = await PartNodeGroup.find();
        res.json(parts);
    } catch (err) {
        res.json({ message: err });
    }
});

module.exports = router;
