const express = require("express");
const router = express.Router();
const Partan = require("../models/part");

// const { start } = require("../../elasticSearchApi2");
const { elasticNeoSearch } = require("../../elasticNeoSearch");
const { partanDiagramsNeoSearch } = require("../../partanDiagramsNeoSearch");

// var elasticsearch = require("elasticsearch");
// var client = new elasticsearch.Client({
//     host: "localhost:9200",
//     // log: "trace",
//     apiVersion: "7.2", // use the same version of your Elasticsearch instance
// });

// client.ping(
//     {
//         // ping usually has a 3000ms timeout
//         requestTimeout: 5000,
//     },
//     function (error) {
//         if (error) {
//             console.trace("elasticsearch cluster is down!");
//         } else {
//             // console.log("All is well");
//         }
//     }
// );

// **********************************************************************************************

router.get("/", async (req, res) => {
    // console.log("tget Paran");
    // try {
    //     const partans = await Partan.find();
    //     res.json(partans);
    // } catch (err) {
    //     res.json({ message: err });
    // }
    res.json({ message: "get is not defind" });
});

// **********************************************************************************************

router.post("/", async (req, res) => {
    console.log("elastic search is started...");
    const b = req.body;
    console.log("b", b);
    const { queryString, filters } = b;
    try {
        let partans = await start(queryString, filters);
        // console.log(partans.genParts)
        partans = res.json(partans);
    } catch (err) {
        console.log(err);
        res.json({ message: err });
    }
});

// **********************************************************************************************

router.post("/s", async (req, res) => {
    console.log("elastic neo4j search is started...");
    const b = req.body;
    console.log("b", b);
    const { queryString, filters } = b;
    try {
        let partans = await elasticNeoSearch(queryString, filters);
        // console.log(partans.genParts)
        partans = res.json(partans);
    } catch (err) {
        console.log(err);
        res.json({ message: err });
    }
});

// **********************************************************************************************

router.post("/d", async (req, res) => {
    console.log("partans diag neo4j search is started...");
    const b = req.body;
    console.log("b", b);
    const { partansArray } = b;
    try {
        let partans = await partanDiagramsNeoSearch(partansArray);
        // console.log(partans.genParts)
        partans = res.json(partans);
    } catch (err) {
        console.log(err);
        res.json({ message: err });
    }
});

// **********************************************************************************************

// router.post("/", async (req, res) => {
//     console.log("router.post(/, async (req, res) => {", req.body);
//     const b = req.body;
//     const partan = new Partan({
//         dataBaseId: b.dataBaseId,
//         name: b.name,
//         partanNumber: b.partanNumber,
//         partanSections: b.partanSections,
//         catalogs: b.catalogs,
//         analogsGroup: b.analogsGroup,
//     });

//     try {
//         const savedPartan = await partan.save();
//         res.json(savedPartan);
//     } catch (err) {
//         res.json({ message: err });
//     }
// });

// **********************************************************************************************

router.post("/insertPartan", async (req, res) => {
    // {
    //   constSec:{ bn: 1, pn:2, }
    //   payloadSec:{}
    // }
    const b = req.body;

    console.log("insertPartan");
    const objectList = b;
    // for (let i in objectList) {
    // const cs = b.constSec;
    // const ps = b.payloadSec;
    const newObject = objectList;
    if (!newObject.payload || !newObject.payload.brandName) {
        // mark it as empry and skip insertion
    } else {
        const bn = newObject.payload.brandName.toUpperCase();
        const pn = newObject.payload.partanNumber
            .replace(/[^a-zA-Zа-яА-Я0-9 ]/g, "")
            .toUpperCase();
        const name = newObject.payload.name;

        const upsertObj = {
            _id: {
                bn: bn,
                pn: pn,
            },
            bn: bn,
            pn: pn,
            pubNam: name,
        };
        console.log(newObject._id);
        // console.log(newObject);
        try {
            const upsRes = await Partan.updateOne(
                { _id: upsertObj._id },
                { $set: { ...upsertObj } },
                { upsert: true }
            );
            console.log(
                "=============================================\n",
                upsRes,
                "\n=============================================",
                "\n"
            );
            // const foundObject = await Partan.findOne({
            //     _id: upsertObj._id,
            // });
            // console.log(foundObject);

            // const pld = foundObject.paylaod ? foundObject.paylaod : {};
            // pld[newObject._id] = { ...newObject };
            // console.log(pld);
            // const insertPayload = await Partan.updateOne(
            //     { _id: upsertObj._id },
            //     { ...foundObject }
            // );
            // console.log(pld);

            // console.log("payload updated", insertPayload);
        } catch (err) {
            console.log(err);
            res.json({ message: err });
        }
        // }
    }
    console.log({ message: "update is ok" });

    res.json({ message: "update is ok" });
});

// **********************************************************************************************

// adding crosses from armtek without tecknical data
router.post("/gettotreat", async (req, res) => {
    const b = req.body;
    const cond = b.cond;
    const limit = b.limit;
    let lock = b.lock;
    if (!lock) lock = 10 * 60000;
    // lock = Number(req.params.count);
    // const count = Number(req.params.count);
    // console.log(count);
    try {
        const foundDocs = await Partan.find({
            ...cond,
            isUnderTreatment: { $lte: new Date().getTime() },
        }).limit(limit);
        if (!foundDocs.length) {
            res.json([]);
            return [];
        }
        const ids = foundDocs.map((item) => {
            return item._id;
        });
        await Partan.updateMany(
            { _id: { $in: ids } },
            { $set: { isUnderTreatment: new Date().getTime() + lock } }
        );
        console.log(foundDocs.length);
        res.json(foundDocs);
    } catch (err) {
        console.log(err);
        res.json({ message: err });
    }
});

// **********************************************************************************************

router.delete("/:partanId", async (req, res) => {
    try {
        const removedPartan = await Partan.remove({ _id: req.params.partanId });

        res.json(removedPartan);
    } catch (err) {
        res.json({ message: err });
    }
});

// **********************************************************************************************

router.patch("/:partanId", async (req, res) => {
    console.log(req.body);
    console.log(req.params.partanId);
    try {
        const updatedPartan = await Partan.updateOne(
            { _id: req.params.partanId },
            { $set: { ...req.body } }
        );

        // console.log("updatedPartan", updatedPartan);
        res.json(updatedPartan);
    } catch (err) {
        res.json({ message: err });
    }
});

module.exports = router;
