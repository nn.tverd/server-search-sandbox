const express = require("express");
const router = express.Router();
const ScoreModel = require("../models/scores");
// **********************************************************************************************
ScoreModel.createMapping(
    {
        analysis: {
            filter: {
                russian_stop: { type: "stop", stopwords: "_russian_" },
                russian_keywords: {
                    type: "keyword_marker",
                    keywords: ["примера"],
                },
                russian_stemmer: {
                    type: "stemmer",
                    language: "russian",
                },
                custom_stem: {
                    type: "stemmer_override",
                    rules_path: "analysis/rules.txt",
                },
            },
            analyzer: {
                rebuilt_russian: {
                    tokenizer: "standard",
                    filter: [
                        "lowercase",
                        "russian_stop",
                        "russian_keywords",
                        "russian_stemmer",
                        "custom_stem",
                    ],
                },
            },
        },
    },
    function (err, mapping) {
        console.log("\n\n\n\n scores ");
        if (err) {
            console.log("error creating mapping!");
            console.log(err);
        } else {
            console.log("mapping is created!");
            // console.log(mapping);
        }
    }
);

const stream = ScoreModel.synchronize();
let count = 0;

stream.on("data", function () {
    count++;
});
stream.on("close", function () {
    console.log(`indexed ${count} documnets`);
});
stream.on("error", function (err) {
    console.log(err);
});

// **********************************************************************************************
// **********************************************************************************************

router.get("/", async (req, res) => {
    console.log("get scores");
    try {
        const scores = await ScoreModel.find();
        res.json(scores);
    } catch (err) {
        res.json({ message: err });
    }
});

// **********************************************************************************************

router.post("/search", async (req, res) => {
  
});

// **********************************************************************************************

router.post("/", async (req, res) => {
    
});

// **********************************************************************************************

router.post("/insertscores", async (req, res) => {
    
});

// **********************************************************************************************

module.exports = router;
