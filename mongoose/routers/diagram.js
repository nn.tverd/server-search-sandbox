const express = require("express");
const router = express.Router();
const Diagram = require("../models/diagram");
const { syncElastic, connectNeo4j } = require("../models/plugins/config");

// **********************************************************************************************
if (syncElastic) {
    Diagram.createMapping(function (err, mapping) {
        console.log("\n\n\n\n Diagram ");
        if (err) {
            console.log("error creating mapping!");
            console.log(err);
        } else {
            console.log("mapping is created!");
            // console.log(mapping);
        }
    });

    const stream = Diagram.synchronize();
    let count = 0;

    stream.on("data", function () {
        count++;
    });
    stream.on("close", function () {
        console.log(`indexed ${count} documnets`);
    });
    stream.on("error", function (err) {
        console.log(err);
    });
}

// **********************************************************************************************

router.get("/", async (req, res) => {
    console.log("get Diagram");
    try {
        const parts = await Diagram.find();
        res.json(parts);
    } catch (err) {
        res.json({ message: err });
    }
});

module.exports = router;
