const express = require("express");
const router = express.Router();
const PartanRel = require("../models/partanRel");
// **********************************************************************************************
PartanRel.createMapping({}, function (err, mapping) {
    console.log("\n\n\n\n PARTANRel ");
    if (err) {
        console.log("error creating mapping!");
        console.log(err);
    } else {
        console.log("mapping is created!");
        // console.log(mapping);
    }
});

const stream = PartanRel.synchronize();
let count = 0;

stream.on("data", function () {
    count++;
});
stream.on("close", function () {
    console.log(`indexed ${count} documnets`);
});
stream.on("error", function (err) {
    console.log(err);
});

// **********************************************************************************************
// **********************************************************************************************

router.get("/", async (req, res) => {
    console.log("get PartanRel");
    try {
        const partanRels = await PartanRel.find();
        res.json(partanRels);
    } catch (err) {
        res.json({ message: err });
    }
});

// **********************************************************************************************

module.exports = router;
