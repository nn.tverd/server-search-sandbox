const axios = require("axios");

function timeout(ms) {
    return new Promise((resolve) => setTimeout(resolve, ms));
}
async function clearPartanRefsInParts() {
    const __parts = await axios({
        method: "get",
        url: "http://localhost:3033/part",
    });

    // console.log(parts);
    const parts = __parts.data;
    console.log("cleaning 1 .......");
    for (let i in parts) {
        console.log("inside parts cycle");
        const part = parts[i];

        const partans = part.partan;
        // console.log("\n\n", i, );
        console.log("cleaning 2 .......");

        let p = 0;
        while (true) {
            if (partans.length <= p) break;
            console.log("cleaning 3 .......");

            let p2 = p + 1;
            while (true) {
                if (partans.length <= p2) break;

                console.log("cleaning 4 .......");

                if (partans[p]._id == partans[p2]._id) {
                    console.log("cleaning 5 .......");

                    partans.splice(p2, 1);
                } else {
                    p2++;
                }
            }
            p++;
        }
        console.log(
            "cleaning 6 .......",
            `http://localhost:3033/part/clearpartan/${part._id}`
        );
        console.log("if( !partans ){", partans);

        if (!partans) {
            console.log("if( !partans ){ - return ", partans);
            return;
        }

        const resp = await axios({
            method: "post",
            url: `http://localhost:3033/part/clearpartan/${part._id}`,
            data: { newPartanArray: partans },
            headers: {
                "Content-Type": "application/json",
            },
        });
        console.log(resp.data);
    }

    return { ok: 1 };
}

clearPartanRefsInParts();
