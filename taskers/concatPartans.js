const axios = require("axios");

function timeout(ms) {
    return new Promise((resolve) => setTimeout(resolve, ms));
}

async function concatPartans() {
    const __parts = await axios({
        method: "get",
        url: "http://localhost:3033/part",
    });

    // console.log(parts);
    const parts = __parts.data;
    for (let i in parts) {
        console.log("inside parts cycle");
        const part = parts[i];

        const partans = part.partan;
        // console.log("\n\n", i, );

        for (let p in partans) {
            console.log("inside partans cycle");
            console.log(p, Number(p) + 1);
            const partan1 = partans[p];
            const partan2 = partans[Number(p) + 1];
            if (!partan2) {
                // console.log("have reached end of partans");
                break;
            }
            if( partan1._id == partan2._id ){
                continue;
            }
            const diff =
                Math.abs(partan1.relScore - partan2.relScore) / partan1.relScore;
            if (diff > 0.2) {
                console.log("diff is less than 0.2");
                continue;
            }
            const data = {
                mainId: partan1._id,
                candidateId: partan2._id,
            };
            await axios({
                method: "post",
                url: "http://localhost:3033/partan/concat2",
                data: data,
                headers: {
                    "Content-Type": "application/json",
                },
            });
            console.log("before timeout", i, p);

            await timeout(10 * 1);
            console.log("after timeout");
            partan2._id = partan1._id;
        }
    }

    return { ok: 1 };
}

concatPartans();
