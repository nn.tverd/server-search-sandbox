const axios = require("axios");
var mongoose = require("mongoose");

var elasticsearch = require("elasticsearch");
const e = require("express");
const partan = require("./mongoose/models/partan");
var client = new elasticsearch.Client({
    host: "localhost:9200",
    // log: "trace",
    apiVersion: "7.2", // use the same version of your Elasticsearch instance
});

client.ping(
    {
        // ping usually has a 3000ms timeout
        requestTimeout: 5000,
    },
    function (error) {
        if (error) {
            console.trace("elasticsearch cluster is down!");
        } else {
            // console.log("All is well");
        }
    }
);

const searchMakes = async (query) => {
    try {
        const makes = await client.search({
            index: "makes",
            q: query,
        });
        console.log("makes.hits.hits");
        console.log(makes.hits.hits);
        console.log("========================");
        return makes.hits.hits;
    } catch (error) {
        console.trace(error.message);
    }
};
const searchModels = async (query) => {
    try {
        const models = await client.search({
            index: "models",
            q: query,
        });
        console.log("models.hits.hits");
        console.log(models.hits.hits);
        console.log("========================");
        return models.hits.hits;
    } catch (error) {
        console.trace(error.message);
    }
};
const searchModifs = async (query) => {
    try {
        const modifs = await client.search({
            index: "modifs",
            q: query,
        });
        console.log("modifs.hits.hits");
        console.log(modifs.hits.hits);
        console.log("========================");
        return modifs.hits.hits;
    } catch (error) {
        console.trace(error.message);
    }
};

const searchPartNodeGroups = async (query) => {
    try {
        const partnodegroups = await client.search({
            index: "v3partnodegroups",
            q: query,
        });
        console.log("partnodegroups.hits.hits");
        console.log(partnodegroups.hits.hits);
        console.log("========================");
        return partnodegroups.hits.hits;
    } catch (error) {
        console.trace(error.message);
    }
};

const searchPartNodes = async (query) => {
    try {
        const partnodes = await client.search({
            index: "v3partnodes",
            q: query,
        });
        console.log("partnodes.hits.hits");
        console.log(partnodes.hits.hits);
        console.log("========================");
        return partnodes.hits.hits;
    } catch (error) {
        console.trace(error.message);
    }
};

const searchGenParts = async (query) => {
    console.log(query);
    try {
        const genparts = await client.search({
            index: "v3genparts",
            q: query,
        });
        console.log("genparts.hits.hits");
        console.log(genparts.hits.hits);
        console.log("========================");
        return genparts.hits.hits;
    } catch (error) {
        console.trace(error.message);
    }
};

const searchPartan = async (_query, _scores, _sq, _filters) => {
    try {
        const body = {
            query: {
                bool: {
                    should: [
                        {
                            nested: {
                                path: "mk",
                                query: {
                                    term: {
                                        "mk.mkId": {
                                            value: _query,
                                        },
                                    },
                                },
                            },
                        },
                        // {
                        //     nested: {
                        //         path: "md",
                        //         query: {
                        //             term: {
                        //                 "md.mdId": {
                        //                     value: _query,
                        //                 },
                        //             },
                        //         },
                        //     },
                        // },
                        // {
                        //     nested: {
                        //         path: "mf",
                        //         query: {
                        //             term: {
                        //                 "mf.mfId": {
                        //                     value: _query,
                        //                 },
                        //             },
                        //         },
                        //     },
                        // },
                    ],
                },
            },
        };
        const body2 = {
            query: {
                nested: {
                    path: "mk",
                    query: {
                        bool: {
                            must: [{ term: { "mk.mkId": _query, boost: 3.0 } }],
                        },
                    },
                },
                nested: {
                    path: "md",
                    query: {
                        bool: {
                            should: [
                                { term: { "md.mdId": _query, boost: 2.0 } },
                            ],
                        },
                    },
                },
                nested: {
                    path: "mf",
                    query: {
                        bool: {
                            should: [
                                { term: { "mf.mfId": _query, boost: 1.0 } },
                            ],
                        },
                    },
                },
            },
        };
        const body3 = {
            query: {
                nested: {
                    path: "mk",
                    query: {
                        multi_match: {
                            query: _query,
                            fields: ["mk.mkIds"],
                        },
                    },
                },
            },
        };
        const body4 = {
            query: {
                nested: {
                    path: "mk",
                    query: {
                        query_string: {
                            default_field: "mk.id",
                            query: _query,
                        },
                    },
                },
                nested: {
                    path: "md",
                    query: {
                        query_string: {
                            default_field: "md.id",
                            query: _query,
                        },
                    },
                },
                nested: {
                    path: "mf",
                    query: {
                        query_string: {
                            default_field: "mf.id",
                            query: _query,
                        },
                    },
                },
                nested: {
                    path: "gp",
                    query: {
                        query_string: {
                            default_field: "gp.id",
                            query: _query,
                        },
                    },
                },
                nested: {
                    path: "nd",
                    query: {
                        query_string: {
                            default_field: "nd.id",
                            query: _query,
                        },
                    },
                },
                nested: {
                    path: "gn",
                    query: {
                        query_string: {
                            default_field: "gn.id",
                            query: _query,
                        },
                    },
                },
            },
        };
        const report = [];
        const mkSearch = [];
        for (let i in _sq.makes) {
            const kboost = _filters.makes.includes(_sq.makes[i].id) ? 10 : 1;
            const item = {
                terms: {
                    "mk.id": [_sq.makes[i].id],
                    boost: _sq.makes[i].score * kboost,
                },
            };
            mkSearch.push(JSON.parse(JSON.stringify(item)));
            item.terms.name = _sq.makes[i].name;
            report.push(item);
            console.log("mkSearch", mkSearch);
        }
        const mdSearch = [];
        for (let i in _sq.models) {
            const kboost = _filters.models.includes(_sq.models[i].id) ? 10 : 1;
            const item = {
                terms: {
                    "md.id": [_sq.models[i].id],
                    boost: _sq.models[i].score * kboost,
                },
            };
            mdSearch.push(JSON.parse(JSON.stringify(item)));
            item.terms.name = _sq.models[i].name;
            report.push(item);
        }
        console.log("mdSearch", mdSearch);

        const mfSearch = [];
        for (let i in _sq.modifs) {
            const kboost = _filters.modifs.includes(_sq.modifs[i].id) ? 10 : 1;
            const item = {
                terms: {
                    "mf.id": [_sq.modifs[i].id],
                    boost: _sq.modifs[i].score * kboost,
                },
            };
            mfSearch.push(JSON.parse(JSON.stringify(item)));
            item.terms.name = _sq.modifs[i].name;
            report.push(item);
        }
        console.log("mfSearch", mfSearch);

        const gnSearch = [];
        for (let i in _sq.v3partnodegroups) {
            const kboost = _filters.partNodeGroups.includes(
                _sq.v3partnodegroups[i].id
            )
                ? 10
                : 1;
            const item = {
                terms: {
                    "gn.id": [_sq.v3partnodegroups[i].id],
                    boost: _sq.v3partnodegroups[i].score * kboost,
                },
            };
            gnSearch.push(JSON.parse(JSON.stringify(item)));
            item.terms.name = _sq.v3partnodegroups[i].name;
            report.push(item);
        }
        console.log("gnSearch", gnSearch);

        const ndSearch = [];
        for (let i in _sq.v3partnodes) {
            const kboost = _filters.partNodes.includes(_sq.v3partnodes[i].id)
                ? 10
                : 1;
            const item = {
                terms: {
                    "nd.id": [_sq.v3partnodes[i].id],
                    boost: _sq.v3partnodes[i].score * kboost,
                },
            };
            ndSearch.push(JSON.parse(JSON.stringify(item)));
            item.terms.name = _sq.v3partnodes[i].name;
            report.push(item);
        }
        console.log("ndSearch", ndSearch);

        const gpSearch = [];
        for (let i in _sq.v3genparts) {
            const kboost = _filters.genParts.includes(_sq.v3genparts[i].id)
                ? 10
                : 1;
            const item = {
                terms: {
                    "gp.id": [_sq.v3genparts[i].id],
                    boost: _sq.v3genparts[i].score * kboost,
                },
            };
            gpSearch.push(JSON.parse(JSON.stringify(item)));
            item.terms.name = _sq.v3genparts[i].name;
            report.push(item);
        }
        console.log("gpSearch", gpSearch);
        console.log("report", JSON.stringify(report));

        const body5 = {
            query: {
                bool: {
                    should: [
                        {
                            nested: {
                                path: "md",
                                query: {
                                    bool: {
                                        should: mdSearch,
                                    },
                                },
                                // boost: _scores.models,
                            },
                        },
                        {
                            nested: {
                                path: "mk",
                                query: {
                                    bool: {
                                        should: mkSearch,
                                    },
                                },
                                boost: _scores.makes,
                            },
                        },
                        {
                            nested: {
                                path: "mf",
                                query: {
                                    bool: {
                                        should: mfSearch,
                                    },
                                },
                                boost: _scores.modifs,
                            },
                        },
                        {
                            nested: {
                                path: "gp",
                                query: {
                                    bool: {
                                        should: gpSearch,
                                    },
                                },
                                boost: _scores.genparts,
                            },
                        },
                        {
                            nested: {
                                path: "gn",
                                query: {
                                    bool: {
                                        should: gnSearch,
                                    },
                                },
                                boost: _scores.partnodegroups,
                            },
                        },
                        {
                            nested: {
                                path: "nd",
                                query: {
                                    bool: {
                                        should: ndSearch,
                                    },
                                },
                                boost: _scores.partnodes,
                            },
                        },
                    ],
                },
            },
        };
        const partans = await client.search({
            index: "partans",
            body: body5,
        });
        console.log("partans.hits.hits", partans.hits.hits.length);
        for (let i in partans.hits.hits) {
            console.log(
                "partans.hits.hits[i]._source",
                partans.hits.hits[i]._source.metaDesc
            );
            console.log(
                "partans.hits.hits[i]._score",
                partans.hits.hits[i]._score
            );
        }
        console.log("========================");
        return partans.hits.hits;
    } catch (error) {
        console.trace(error.message);
    }
};

(async function _start() {
    return;
    const q = "колодки задние";

    // const q = "КОМПЛЕКТ ПЕРЕДНИХ КОЛОДОК chevrolet aveo 2011";
    const makes = searchMakes(q);
    const models = searchModels(q);
    const modifs = searchModifs(q);
    const partNodeGroups = searchPartNodeGroups(q);
    const partNodes = searchPartNodes(q);
    const genParts = searchGenParts(q);
    const consolidated = (await makes)
        .concat(await models)
        .concat(await modifs)
        .concat(await partNodeGroups)
        .concat(await partNodes)
        .concat(await genParts);
    // const consolidated = await makes;

    console.log(consolidated);
    let string = "";
    const ids = [];
    const scores = {
        makes: 1,
        models: 1,
        modifs: 1,
        // partnodegroups: 1,
        // partnodes: 1,
        // genparts: 1,
        v3partnodegroups: 1,
        v3partnodes: 1,
        v3genparts: 1,
    };
    const sq = {
        makes: [],
        models: [],
        modifs: [],
        // partnodegroups: [],
        // partnodes: [],
        // genparts: [],
        v3partnodegroups: [],
        v3partnodes: [],
        v3genparts: [],
    };
    for (let i in consolidated) {
        string += consolidated[i]._id + " ";
        scores[consolidated[i]._index] += consolidated[i]._score;
        sq[consolidated[i]._index].push({
            score: 10 * consolidated[i]._score,
            // score: 1 ,

            id: consolidated[i]._id,
            name: consolidated[i]._source.name,
        });

        ids.push(consolidated[i]._id);
        // scores[consolidated[i]._id] = consolidated[i]._score;
    }
    // console.log(string);
    // console.log(ids);
    console.log(scores);

    const partans = searchPartan(ids, scores, sq);
    //     const partans = await axios({
    //         method: "post",
    //         url: `http://localhost:3031/partan/search`,
    //         data: {
    //             cond: { "mk.mkId": { $in: ids } },
    //             scores: scores,
    //         },
    //         headers: {
    //             "Content-Type": "application/json",
    //         },
    //     });
    //     console.log(partans.data);
})();

async function _start2(queryString, filters) {
    const q = queryString;
    console.log(q);
    // const q = "КОМПЛЕКТ ПЕРЕДНИХ КОЛОДОК chevrolet aveo 2011";
    const makes = searchMakes(q);
    const models = searchModels(q);
    const modifs = searchModifs(q);
    const partNodeGroups = searchPartNodeGroups(q);
    const partNodes = searchPartNodes(q);
    const genParts = searchGenParts(q);

    //*****************************************************************************
    //*****************************************************************************
    //*****************************************************************************
    //*****************************************************************************
    const ids = [];

    // +++++++++++++++++++++++++++++++++++++++++
    (function getIds2(gp, nd, mf, md, ids) {
        const _ids = {};
        // genparts
        for (let i in gp) {
            const item = gp[i];
            if (!_ids[item._source.partNodeGroupId]) {
                _ids[item._source.partNodeGroupId] = {
                    id: item._source.partNodeGroupId,
                    score: item._score,
                };
            } else {
                _ids[item._source.partNodeGroupId].score += item._score;
            }

            if (!_ids[item._source.partNodeId]) {
                _ids[item._source.partNodeId] = {
                    id: item._source.partNodeId,
                    score: item._score,
                };
            } else {
                _ids[item._source.partNodeId].score += item._score;
            }

            // ids.push(item._source.partNodeGroupId);
            // ids.push(item._source.partNodeId);
        }
        // modes
        for (let i in nd) {
            const item = gp[i];
            // ids.push(item._source.partNodeGroupId);
            if (!_ids[item._source.partNodeGroupId]) {
                _ids[item._source.partNodeGroupId] = {
                    id: item._source.partNodeGroupId,
                    score: item._score,
                };
            } else {
                _ids[item._source.partNodeGroupId].score += item._score;
            }
        }
        // modifs
        for (let i in mf) {
            const item = mf[i];
            if (!_ids[item._source.makeId]) {
                _ids[item._source.makeId] = {
                    id: item._source.makeId,
                    score: item._score,
                };
            } else {
                _ids[item._source.makeId].score += item._score;
            }

            if (!_ids[item._source.modelId]) {
                _ids[item._source.modelId] = {
                    id: item._source.modelId,
                    score: item._score,
                };
            } else {
                _ids[item._source.modelId].score += item._score;
            }

            // ids.push(item._source.partNodeGroupId);
            // ids.push(item._source.partNodeId);
        }
        // models
        for (let i in md) {
            const item = md[i];
            if (!_ids[item._source.makeId]) {
                _ids[item._source.makeId] = {
                    id: item._source.makeId,
                    score: item._score,
                };
            } else {
                _ids[item._source.makeId].score += item._score;
            }
        }
        for (let i in _ids) {
            ids.push(_ids[i]);
        }
    })(await genParts, await partNodes, await modifs, await models, ids);
    // *****************************************************************************
    // reverse search
    console.log("ids", ids);
    // GET /_search
    const idsQ = {
        query: {
            bool: {
                should: [],
            },
        },
    };
    for (let i in ids) {
        idsQ.query.bool.should.push({
            term: {
                _id: {
                    value: ids[i].id,
                    boost: ids[i].score,
                },
            },
        });
    }
    console.log(JSON.stringify(idsQ));
    const aditionalSearch = await client.search({
        index: ["v3partnodes", "v3partnodegroups", "makes", "models"],
        // index: "v3partnodes",
        body: idsQ,
    });
    console.log("aditionalSearch", aditionalSearch.hits.hits);
    // *****************************************************************************

    const __partNodes = await partNodes;
    const ___partNodes = {};
    for (let i in __partNodes) {
        if (!___partNodes[__partNodes[i]._id]) {
            ___partNodes[__partNodes[i]._id] = {
                ...__partNodes[i],
            };
        } else {
            ___partNodes[__partNodes[i]._id]._score += __partNodes[i]._score;
        }
    }
    for (let i in aditionalSearch.hits.hits) {
        const hit = aditionalSearch.hits.hits[i];
        if (hit._index !== "v3partnodes") continue;
        if (!___partNodes[hit._id]) {
            ___partNodes[hit._id] = {
                ...hit,
            };
        } else {
            ___partNodes[hit._id]._score += hit._score;
        }
    }
    // +++++++++++++++++++++++++++++++++++++++++

    const __partNodeGroups = await partNodeGroups;
    const ___partNodeGroups = {};
    for (let i in __partNodeGroups) {
        if (!___partNodeGroups[__partNodeGroups[i]._id]) {
            ___partNodeGroups[__partNodeGroups[i]._id] = {
                ...__partNodeGroups[i],
            };
        } else {
            ___partNodeGroups[__partNodeGroups[i]._id]._score +=
                __partNodeGroups[i]._score;
        }
    }
    for (let i in aditionalSearch.hits.hits) {
        const hit = aditionalSearch.hits.hits[i];
        if (hit._index !== "v3partnodegroups") continue;

        if (!___partNodeGroups[hit._id]) {
            ___partNodeGroups[hit._id] = {
                ...hit,
            };
        } else {
            ___partNodeGroups[hit._id]._score += hit._score;
        }
    }
    // +++++++++++++++++++++++++++++++++++++++++

    const __makes = await makes;
    const ___makes = {};
    for (let i in __makes) {
        if (!___makes[__makes[i]._id]) {
            ___makes[__makes[i]._id] = {
                ...__makes[i],
            };
        } else {
            ___makes[__makes[i]._id]._score += __makes[i]._score;
        }
    }
    for (let i in aditionalSearch.hits.hits) {
        const hit = aditionalSearch.hits.hits[i];
        if (hit._index !== "makes") continue;

        if (!___makes[hit._id]) {
            ___makes[hit._id] = {
                ...hit,
            };
        } else {
            ___makes[hit._id]._score += hit._score;
        }
    }
    // +++++++++++++++++++++++++++++++++++++++++

    const __models = await models;
    const ___models = {};
    for (let i in __models) {
        if (!___models[__models[i]._id]) {
            ___models[__models[i]._id] = {
                ...__models[i],
            };
        } else {
            ___models[__models[i]._id]._score += __models[i]._score;
        }
    }
    for (let i in aditionalSearch.hits.hits) {
        const hit = aditionalSearch.hits.hits[i];
        if (hit._index !== "models") continue;

        if (!___models[hit._id]) {
            ___models[hit._id] = {
                ...hit,
            };
        } else {
            ___models[hit._id]._score += hit._score;
        }
    }
    // +++++++++++++++++++++++++++++++++++++++++
    // console.log(___partNodeGroups, ___partNodes);
    const sendedPartNodeGroups = [];
    for (let i in ___partNodeGroups) {
        sendedPartNodeGroups.push(___partNodeGroups[i]);
    }
    const sendedPartNodes = [];
    for (let i in ___partNodes) {
        sendedPartNodes.push(___partNodes[i]);
    }
    const sendedMakes = [];
    for (let i in ___makes) {
        sendedMakes.push(___makes[i]);
    }
    const sendedModels = [];
    for (let i in ___models) {
        sendedModels.push(___models[i]);
    }
    //*****************************************************************************
    //*****************************************************************************
    //*****************************************************************************

    const consolidated = sendedMakes
        .concat(sendedModels)
        .concat(await modifs)
        .concat(sendedPartNodeGroups)
        .concat(sendedPartNodes)
        .concat(await genParts);
    // const consolidated = await makes;

    console.log(consolidated);
    let string = "";
    const scores = {
        makes: 1,
        models: 1,
        modifs: 1,
        // partnodegroups: 1,
        // partnodes: 1,
        // genparts: 1,
        v3partnodegroups: 1,
        v3partnodes: 1,
        v3genparts: 1,
    };
    const sq = {
        makes: [],
        models: [],
        modifs: [],
        // partnodegroups: [],
        // partnodes: [],
        // genparts: [],
        v3partnodegroups: [],
        v3partnodes: [],
        v3genparts: [],
    };
    for (let i in consolidated) {
        string += consolidated[i]._id + " ";
        scores[consolidated[i]._index] += consolidated[i]._score;
        sq[consolidated[i]._index].push({
            score: 10 * consolidated[i]._score,
            // score: 1 ,

            id: consolidated[i]._id,
            name: consolidated[i]._source.name,
        });

        // ids.push(consolidated[i]._id);
        // scores[consolidated[i]._id] = consolidated[i]._score;
    }
    // console.log(string);
    // console.log(ids);
    // console.log(scores);

    const partans = await searchPartan(ids, scores, sq, filters);
    // console.log(partans);

    // ------------ cars extraction from partans ++++++++++++++++++++++++++++++++++

    function updateCarsResults(partans) {
        let carEntIds = [];
        for (let e in partans) {
            // if(e>1) continue;
            const partan = partans[e];
            let ptnIds = [];
            // console.log("function updateCarsResults(partans) {", partan);
            ptnIds = ptnIds.concat(partan._source.mk);
            ptnIds = ptnIds.concat(partan._source.md);
            ptnIds = ptnIds.concat(partan._source.mf);
            // console.log("partan._score", partan._score);
            for (let idx in ptnIds) {
                if (ptnIds[idx].relScore > 0) {
                    ptnIds[idx].relScore =
                        (ptnIds[idx].relScore + partan._score) * 0.0005;
                } else {
                    ptnIds[idx].relScore = 0;
                }
            }
            carEntIds = carEntIds.concat(JSON.parse(JSON.stringify(ptnIds)));
        }
        return JSON.parse(JSON.stringify(carEntIds));
    }
    const ptnsCarIds = updateCarsResults(partans);
    const idsQ2 = {
        query: {
            bool: {
                should: [],
            },
        },
    };
    for (let i in ptnsCarIds) {
        idsQ2.query.bool.should.push({
            term: {
                _id: {
                    value: ptnsCarIds[i].id,
                    boost: ptnsCarIds[i].relScore,
                },
            },
        });
    }
    // console.log("\n\n\n\nJSON.stringify(idsQ2)", JSON.stringify(idsQ2));
    const carUpdatesResults = await client.search({
        index: ["modifs", "makes", "models"],
        // index: "v3partnodes",
        body: idsQ2,
    });

    console.log("carUpdatesResults", carUpdatesResults.hits.hits);

    // **********************************************************
    const ___modifs = {};
    for (let i in carUpdatesResults.hits.hits) {
        const hit = carUpdatesResults.hits.hits[i];
        if (hit._index === "makes") {
            if (!___makes[hit._id]) {
                ___makes[hit._id] = {
                    ...hit,
                };
            } else {
                ___makes[hit._id]._score += hit._score;
            }
        }
        if (hit._index === "models") {
            if (!___models[hit._id]) {
                ___models[hit._id] = {
                    ...hit,
                };
            } else {
                ___models[hit._id]._score += hit._score;
            }
        }
        if (hit._index === "modifs") {
            if (!___modifs[hit._id]) {
                ___modifs[hit._id] = {
                    ...hit,
                };
            } else {
                ___modifs[hit._id]._score += hit._score;
            }
        }
    }

    const _sendedModifs = [];
    for (let i in ___modifs) {
        _sendedModifs.push(___modifs[i]);
    }
    const _sendedMakes = [];
    for (let i in ___makes) {
        _sendedMakes.push(___makes[i]);
    }
    const _sendedModels = [];
    for (let i in ___models) {
        _sendedModels.push(___models[i]);
    }
    console.log(_sendedMakes, _sendedModels, _sendedModifs);
    // ------------ cars extraction from partans ----------------------------------

    return {
        partans: partans,
        // makes: await makes,
        // models: await models,
        makes: _sendedMakes,
        models: _sendedModels,
        modifs: _sendedModifs,
        // partNodeGroups: await partNodeGroups,
        // partNodes: await partNodes,
        partNodeGroups: sendedPartNodeGroups,
        partNodes: sendedPartNodes,
        genParts: await genParts,
    };
    //     const partans = await axios({
    //         method: "post",
    //         url: `http://localhost:3031/partan/search`,
    //         data: {
    //             cond: { "mk.mkId": { $in: ids } },
    //             scores: scores,
    //         },
    //         headers: {
    //             "Content-Type": "application/json",
    //         },
    //     });
    //     console.log(partans.data);
}

module.exports.start = _start2;
