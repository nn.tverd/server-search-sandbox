var elasticsearch = require("elasticsearch");

const util = require("util");

const genPart = require("./mongoose/models/genPart");
const partNode = require("./mongoose/models/partNode");
const partNodeGroup = require("./mongoose/models/partNodeGroup");

const carMake = require("./mongoose/models/carMake");
const carModel = require("./mongoose/models/carModel");
const carModif = require("./mongoose/models/carModif");

const partan = require("./mongoose/models/partan");
// ===========================================================================
var client = new elasticsearch.Client({
    host: "localhost:9200",
    // log: "trace",
    apiVersion: "7.2", // use the same version of your Elasticsearch instance
});

client.ping(
    {
        // ping usually has a 3000ms timeout
        requestTimeout: 5000,
    },
    function (error) {
        if (error) {
            console.trace("elasticsearch cluster is down!");
        } else {
            // console.log("All is well");
        }
    }
);
// ===========================================================================

function searchFilters(Schema, filters) {
    return new Promise((resolve, reject) => {
        const x = Schema.search(
            {
                ids: {
                    values: filters,
                    boost: 33,
                },
            },
            function (err, results) {
                if (err) {
                    console.log(err);
                    reject(err);
                }
                console.log("IDS: ");
                console.log(
                    util.inspect(results.hits.hits, {
                        showHidden: false,
                        depth: null,
                    })
                );
                let _gps = results.hits;
                resolve(_gps);
            }
        );
    });
}

function searchSimpleQuery(Schema, queryString) {
    return new Promise((resolve, reject) => {
        const x = Schema.search(
            // {
            //     query_string: {
            //         query: queryString,
            //         fields: [
            //             "positionalTags^2",
            //             "tags",
            //             "units^0.5",
            //             "name^3",
            //             "year^1",
            //             "syns^1",
            //         ],
            //     },
            // },
            {
                bool: {
                    should: [
                        {
                            match: {
                                positionalTags: {
                                    query: queryString,
                                    boost: 2.0,
                                },
                            },
                        },
                        {
                            match: {
                                name: {
                                    query: queryString,
                                    boost: 3.0,
                                },
                            },
                        },
                        {
                            match_phrase: {
                                name: {
                                    query: queryString,
                                    boost: 5.0,
                                },
                            },
                        },

                        {
                            match: {
                                syns: {
                                    query: queryString,
                                    boost: 3.0,
                                },
                            },
                        },
                        {
                            match_phrase: {
                                syns: {
                                    query: queryString,
                                    boost: 5.0,
                                },
                            },
                        },
                        {
                            match: {
                                year: {
                                    query: queryString,
                                    boost: 2.0,
                                },
                            },
                        },
                        {
                            match: {
                                units: {
                                    query: queryString,
                                    boost: 0.5,
                                },
                            },
                        },
                        {
                            match: {
                                tags: {
                                    query: queryString,
                                    boost: 1.0,
                                },
                            },
                        },
                    ],
                },
            },
            function (err, results) {
                if (err) {
                    console.log(err);
                    reject(err);
                }
                console.log(
                    "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%  searchSimpleQuery"
                );
                console.log(
                    util.inspect(results.hits.hits, {
                        showHidden: false,
                        depth: null,
                    })
                );

                let _gps = results.hits;
                resolve(_gps);
            }
        );
    });
}

function searchPartansAndPathes(firstSearchResults, meta) {
    return new Promise((resolve, reject) => {
        const firstIds = [];
        for (let i in firstSearchResults) {
            const indx = firstSearchResults[i];
            for (let i in indx) {
                firstIds.push(i);
            }
        }
        // -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
        // get

        const firstIdsStr = JSON.stringify(firstIds);
        let carCypherQuery = `match (ptn:Partan)-[r1:${meta[0].label}]->(${meta[0].name})-[r2:${meta[1].label}]->(${meta[1].name})-[r3:${meta[2].label}]->(${meta[2].name}) `;
        // let carCypherQuery = `match (ptn:Partan)-[r1]->(${meta[0].name})-[r2]->(${meta[1].name})-[r3]->(${meta[2].name}) `;
        carCypherQuery += ` where ${meta[0].name}.m_id in ${firstIdsStr} `;
        carCypherQuery += ` or ${meta[1].name}.m_id in ${firstIdsStr} `;
        carCypherQuery += ` or ${meta[2].name}.m_id in ${firstIdsStr} `;
        carCypherQuery += ` return ptn, ${meta[0].name}, ${meta[1].name}, ${meta[2].name} , r1, apoc.create.vRelationship(ptn, "R2",{score: toFloat(r1.score*r2.score)*0.01}, ${meta[1].name}) as r2, apoc.create.vRelationship(ptn, "R3",{score: toFloat(r1.score*r2.score*r3.score)*0.0001}, ${meta[2].name}) as r3 `;

        console.log("carCypherQuery: ", carCypherQuery);
        partan
            .cypherQuery(carCypherQuery)
            .then((result) => {
                let x = "%";
                for( let i=0;i++;i<10000 ){
                    x += "%"
                }
                console.log(x, result);
                resolve(result);
            })
            .catch((error) => {
                // console.log(error);
                reject(error);
            });
    });
}

function updateNeoPathes(neoResults, meta) {
    return new Promise((resolve, reject) => {
        const Ids = Object.keys(neoResults.partans);

        const IdsStr = JSON.stringify(Ids);
        let carCypherQuery = `match (ptn:Partan)-[r1:${meta[0].label}]->(${meta[0].name})-[r2:${meta[1].label}]->(${meta[1].name})-[r3:${meta[2].label}]->(${meta[2].name}) `;
        // let carCypherQuery = `match (ptn:Partan)-[r1]->(${meta[0].name})-[r2]->(${meta[1].name})-[r3]->(${meta[2].name}) `;
        carCypherQuery += ` where ptn.m_id in ${IdsStr} `;
        carCypherQuery += ` return ptn, ${meta[0].name}, ${meta[1].name}, ${meta[2].name} , r1, apoc.create.vRelationship(ptn, "R2",{score: toFloat(r1.score*r2.score)*0.01}, ${meta[1].name}) as r2, apoc.create.vRelationship(ptn, "R3",{score: toFloat(r1.score*r2.score*r3.score)*0.0001}, ${meta[2].name}) as r3 `;

        // console.log("carCypherQuery: ", carCypherQuery);
        partan
            .cypherQuery(carCypherQuery)
            .then((result) => {
                // console.log(result);
                resolve(result);
            })
            .catch((error) => {
                // console.log(error);
                reject(error);
            });
    });
}

function convirtESHitsToObject(hits, object) {
    for (let i in hits) {
        const hit = hits[i];
        if (!object[hit._index]) {
            object[hit._index] = {};
        }
        if (!object[hit._index][hit._id]) {
            object[hit._index][hit._id] = hit;
        } else {
            object[hit._index][hit._id]._score += hit._score;
        }
    }
}

function extractPartansFromNeo4jRes(neo4jRes1, partansFromNeo, generalResults) {
    // console.log(
    //     "function extractPartansFromNeo4jRes(neo4jRes1, partansFromNeo, generalResults) {",
    //     generalResults
    // );
    const { partans, otherNodes } = partansFromNeo;
    console.log(neo4jRes1.records.length);
    for (let i in neo4jRes1.records) {
        const record = neo4jRes1.records[i];
        const pIndex = 0;
        const n1Index = 1;
        const n2Index = 2;
        const n3Index = 3;
        const r1Index = 4;
        const r2Index = 5;
        const r3Index = 6;
        const partanNode = record._fields[pIndex];
        const n1 = record._fields[n1Index];
        const n2 = record._fields[n2Index];
        const n3 = record._fields[n3Index];
        const _id = partanNode.properties.m_id;
        if (!partans[_id]) {
            partans[_id] = {
                _id: _id,
                score: 0,
            };
        }

        const n1_m_id = n1.properties.m_id;
        const n2_m_id = n2.properties.m_id;
        const n3_m_id = n3.properties.m_id;
        partans[_id].score +=
            Number(record._fields[r1Index].properties.score) * 0.01;
        partans[_id].score +=
            Number(record._fields[r2Index].properties.score) * 0.01;
        partans[_id].score +=
            Number(record._fields[r3Index].properties.score) * 0.01;
        for (let j in generalResults) {
            const grIndex = generalResults[j];
            partans[_id].score += grIndex[n1_m_id]
                ? grIndex[n1_m_id]._score
                : 0;
            partans[_id].score += grIndex[n2_m_id]
                ? grIndex[n2_m_id]._score
                : 0;
            partans[_id].score += grIndex[n3_m_id]
                ? grIndex[n3_m_id]._score
                : 0;
        }
        if (!otherNodes[n1_m_id]) {
            otherNodes[n1_m_id] = {
                _id: n1_m_id,
                score: 0,
            };
        }
        if (!otherNodes[n2_m_id]) {
            otherNodes[n2_m_id] = {
                _id: n2_m_id,
                score: 0,
            };
        }
        if (!otherNodes[n3_m_id]) {
            otherNodes[n3_m_id] = {
                _id: n3_m_id,
                score: 0,
            };
        }
        const ptnIndex = generalResults.partans;
        // console.log(ptnIndex);
        let updateScore = 0;
        if (ptnIndex) {
            updateScore = ptnIndex[_id] ? ptnIndex[_id]._score * 0.01 : 0;
        }
        // console.log("updateScore", n1_m_id, n2_m_id, n3_m_id, updateScore);
        otherNodes[n1_m_id].score +=
            Number(record._fields[r1Index].properties.score) * 0.01 +
            updateScore;
        otherNodes[n2_m_id].score +=
            Number(record._fields[r2Index].properties.score) * 0.01 +
            updateScore;
        otherNodes[n3_m_id].score +=
            Number(record._fields[r3Index].properties.score) * 0.01 +
            updateScore;
    }
}

function searchElasticWithNeoIds(ids) {
    const query = {
        query: {
            bool: {
                should: [],
            },
        },
    };
    for (let i in ids) {
        query.query.bool.should.push({
            term: {
                _id: {
                    value: ids[i]._id,
                    boost: ids[i].score,
                },
            },
        });
    }
    console.log("query", JSON.stringify(query));
    const carUpdatesResults = client.search({
        index: [
            "modifs",
            "makes",
            "models",
            "v3partnodegroups",
            "v3partnodes",
            "v3genparts",
            "partans",
        ],
        // index: "v3partnodes",
        body: query,
        size: 100,
    });
    return carUpdatesResults;
}

async function elasticNeoSearch(queryString, filters) {
    console.log("elastic neo4j search is started...");
    // -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
    // filters search

    const gpf = searchFilters(genPart, filters);
    const ndf = searchFilters(partNode, filters);
    const gnf = searchFilters(partNodeGroup, filters);

    const mkf = searchFilters(carMake, filters);
    const mdf = searchFilters(carModel, filters);
    const mff = searchFilters(carModif, filters);

    // -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
    const gps1 = searchSimpleQuery(genPart, queryString);
    const nds1 = searchSimpleQuery(partNode, queryString);
    const gns1 = searchSimpleQuery(partNodeGroup, queryString);

    const mks1 = searchSimpleQuery(carMake, queryString);
    const mds1 = searchSimpleQuery(carModel, queryString);
    const mfs1 = searchSimpleQuery(carModif, queryString);

    let results1 = await Promise.all([
        gps1,
        nds1,
        gns1,
        mks1,
        mds1,
        mfs1,
        gpf,
        ndf,
        gnf,
        mkf,
        mdf,
        mff,
    ]);

    // console.log(results1);

    generalResults = {};
    for (let i in results1) {
        const result = results1[i];
        convirtESHitsToObject(result.hits, generalResults);
    }
    // -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -

    // to extract partans
    const meta1 = [
        { name: "mf", label: "MF_PT" },
        { name: "md", label: "MD_MF" },
        { name: "mk", label: "MK_MD" },
    ];
    const meta2 = [
        { name: "gp", label: "GP_PT" },
        { name: "nd", label: "ND_GP" },
        { name: "gn", label: "GN_ND" },
    ];
    const neo4jRes1 = await searchPartansAndPathes(generalResults, meta1);
    const neo4jRes2 = await searchPartansAndPathes(generalResults, meta2);

    // console.log("\n\n\n neo4jRes", neo4jRes1, neo4jRes2);

    const partansFromNeo = {
        partans: {},
        otherNodes: {},
    };

    extractPartansFromNeo4jRes(neo4jRes1, partansFromNeo, generalResults);
    extractPartansFromNeo4jRes(neo4jRes2, partansFromNeo, generalResults);

    const updateRes1 = await searchElasticWithNeoIds(partansFromNeo.partans);
    convirtESHitsToObject(updateRes1.hits.hits, generalResults);

    const neo4jRes3 = await updateNeoPathes(partansFromNeo, meta1);
    const neo4jRes4 = await updateNeoPathes(partansFromNeo, meta2);

    extractPartansFromNeo4jRes(neo4jRes3, partansFromNeo, generalResults);
    extractPartansFromNeo4jRes(neo4jRes4, partansFromNeo, generalResults);

    // console.log(partansFromNeo);

    const updateRes2 = await searchElasticWithNeoIds(partansFromNeo.otherNodes);
    const updateRes5 = await searchElasticWithNeoIds(partansFromNeo.partans);

    // console.log(updateRes1.hits.hits);
    // console.log(updateRes5.hits.hits);
    convirtESHitsToObject(updateRes2.hits.hits, generalResults);
    convirtESHitsToObject(updateRes5.hits.hits, generalResults);

    // console.log(generalResults);

    // score partans
    // extract navigational entities
    // score navigational entities

    return generalResults;
}

module.exports.elasticNeoSearch = elasticNeoSearch;
