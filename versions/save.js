const _ = require('lodash');
const helper = require('./helper');
const node = require('./node');

module.exports = {

  one(schema, doc, next, driver) {
    const neo_doc = node.convertToNeoModel(schema, doc);

    let properties = ``;
    let counter = 0;
    for( let i in neo_doc.props ){
      if( counter !== 0 ) properties +=", ";
      counter ++;
      properties += `doc.${i} = ${helper.valueToPlainString(neo_doc.props[i])}`
    }

    console.log( "one(schema, doc, next, driver) {", helper.getLabel(doc), helper.toPlainString(neo_doc.props.name), properties);
    let neo_query = 
    `MERGE (doc:${helper.getLabel(doc)} { m_id: '${neo_doc.props.m_id}' })     
    ON CREATE SET ${properties}
    ON MATCH SET ${properties} `;


    // let neo_query = `CREATE (doc:${helper.getLabel(doc)} ${helper.toPlainString(neo_doc.props)}) `;
    neo_query += this.writeSubDocumentsToCypher(neo_doc.subdocs, '');
    neo_query += this.writeRelationshipsToCypher(neo_doc.rels, '');

    const session = driver.session();
    session.run(neo_query).catch((err) => {
      helper.endMiddleware(next, session, err);
    }).then(() => helper.endMiddleware(next, session));
  },

  multiple(schema, docs, next, driver) {
    console.log( "multiple(schema, docs, next, driver) {s" );
    let neo_query = '';

    _.forEach(docs, (doc, index) => {
      const neo_doc = node.convertToNeoModel(schema, doc);
      const doc_identifier = helper.romanize(index+1);

      neo_query += `CREATE (doc${doc_identifier}:${helper.getLabel(doc)} ${helper.toPlainString(neo_doc.props)}) `;
      neo_query += this.writeSubDocumentsToCypher(neo_doc.subdocs, doc_identifier);
      neo_query += this.writeRelationshipsToCypher(neo_doc.rels, doc_identifier);

    });

    const session = driver.session();
    session.run(neo_query).catch((err) => {
      helper.endMiddleware(next, session, err);
    }).then(() => helper.endMiddleware(next, session));
  },

  writeSubDocumentsToCypher(subdocs, doc_identifier) {
    let neo_query = '';

    _.forEach(subdocs, (subdoc) => {
        neo_query += `CREATE (doc${doc_identifier})-[:${subdoc.rel_name}]->(:${subdoc.label} ${helper.toPlainString(subdoc.properties)}) `;
    });

    return neo_query;
  },

  writeRelationshipsToCypher(relationships, doc_identifier) {
    console.log(  "writeRelationshipsToCypher(relationships, doc_identifier) {", relationships);
    let neo_query = '';
    _.forEach(relationships, (rel, index) => {
        const identifier = helper.romanize(index+1);
        const properties = rel.rel_props ? ` ${helper.toPlainString(rel.rel_props)}` : '';

        let properties2 = ``;
        let counter = 0;
        for( let i in rel.rel_props ){
          if( counter !== 0 ) properties2 +=", ";
          counter ++;
          properties2 += `r.${i} = ${helper.valueToPlainString(rel.rel_props[i])}`
        }

        neo_query += `WITH doc${doc_identifier} `;
        neo_query += `MATCH (${identifier}:${rel.rel_label}) WHERE ${identifier}.m_id = '${rel.m_id}' `;
        neo_query += `MERGE (doc${doc_identifier})-[r:${rel.rel_name}]->(${identifier}) `;
        neo_query += ` ON CREATE SET  ${properties2}`;
        neo_query += ` ON MATCH SET  ${properties2}`;
        // neo_query += `ON MATCH (doc${doc_identifier})-[:${rel.rel_name}${properties}]->(${identifier}) `;
    });

    return neo_query;
  }

};
