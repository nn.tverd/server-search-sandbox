const axios = require("axios");
var mongoose = require("mongoose");

var elasticsearch = require("elasticsearch");
var client = new elasticsearch.Client({
    host: "localhost:9200",
    // log: "trace",
    apiVersion: "7.2", // use the same version of your Elasticsearch instance
});

client.ping(
    {
        // ping usually has a 3000ms timeout
        requestTimeout: 5000,
    },
    function (error) {
        if (error) {
            console.trace("elasticsearch cluster is down!");
        } else {
            // console.log("All is well");
        }
    }
);

const searchMakes = async (query) => {
    try {
        const makes = await client.search({
            index: "makes",
            q: query,
        });
        console.log("makes.hits.hits");
        console.log(makes.hits.hits);
        console.log("========================");
        return makes.hits.hits;
    } catch (error) {
        console.trace(error.message);
    }
};
const searchModels = async (query) => {
    try {
        const models = await client.search({
            index: "models",
            q: query,
        });
        console.log("models.hits.hits");
        console.log(models.hits.hits);
        console.log("========================");
        return models.hits.hits;
    } catch (error) {
        console.trace(error.message);
    }
};
const searchModifs = async (query) => {
    try {
        const modifs = await client.search({
            index: "modifs",
            q: query,
        });
        console.log("modifs.hits.hits");
        console.log(modifs.hits.hits);
        console.log("========================");
        return modifs.hits.hits;
    } catch (error) {
        console.trace(error.message);
    }
};

const searchPartNodeGroups = async (query) => {
    try {
        const partnodegroups = await client.search({
            index: "partnodegroups",
            q: query,
        });
        console.log("partnodegroups.hits.hits");
        console.log(partnodegroups.hits.hits);
        console.log("========================");
        return partnodegroups.hits.hits;
    } catch (error) {
        console.trace(error.message);
    }
};

const searchPartNodes = async (query) => {
    try {
        const partnodes = await client.search({
            index: "partnodes",
            q: query,
        });
        console.log("partnodes.hits.hits");
        console.log(partnodes.hits.hits);
        console.log("========================");
        return partnodes.hits.hits;
    } catch (error) {
        console.trace(error.message);
    }
};

const searchGenParts = async (query) => {
    try {
        const genparts = await client.search({
            index: "genparts",
            q: query,
        });
        console.log("genparts.hits.hits");
        console.log(genparts.hits.hits);
        console.log("========================");
        return genparts.hits.hits;
    } catch (error) {
        console.trace(error.message);
    }
};

const searchPartan = async (_query, _scores) => {
    try {
        const body = {
            query: {
                bool: {
                    should: [
                        {
                            nested: {
                                path: "mk",
                                query: {
                                    term: {
                                        "mk.mkId": {
                                            value: _query,
                                        },
                                    },
                                },
                            },
                        },
                        // {
                        //     nested: {
                        //         path: "md",
                        //         query: {
                        //             term: {
                        //                 "md.mdId": {
                        //                     value: _query,
                        //                 },
                        //             },
                        //         },
                        //     },
                        // },
                        // {
                        //     nested: {
                        //         path: "mf",
                        //         query: {
                        //             term: {
                        //                 "mf.mfId": {
                        //                     value: _query,
                        //                 },
                        //             },
                        //         },
                        //     },
                        // },
                    ],
                },
            },
        };
        const body2 = {
            query: {
                nested: {
                    path: "mk",
                    query: {
                        bool: {
                            must: [{ term: { "mk.mkId": _query, boost: 3.0 } }],
                        },
                    },
                },
                nested: {
                    path: "md",
                    query: {
                        bool: {
                            should: [
                                { term: { "md.mdId": _query, boost: 2.0 } },
                            ],
                        },
                    },
                },
                nested: {
                    path: "mf",
                    query: {
                        bool: {
                            should: [
                                { term: { "mf.mfId": _query, boost: 1.0 } },
                            ],
                        },
                    },
                },
            },
        };
        const body3 = {
            query: {
                nested: {
                    path: "mk",
                    query: {
                        multi_match: {
                            query: _query,
                            fields: ["mk.mkIds"],
                        },
                    },
                },
            },
        };
        const body4 = {
            query: {
                nested: {
                    path: "mk",
                    query: {
                        query_string: {
                            default_field: "mk.mkId",
                            query: _query,
                        },
                    },
                },
                nested: {
                    path: "md",
                    query: {
                        query_string: {
                            default_field: "md.mdId",
                            query: _query,
                        },
                    },
                },
                nested: {
                    path: "mf",
                    query: {
                        query_string: {
                            default_field: "mf.mfId",
                            query: _query,
                        },
                    },
                },
                nested: {
                    path: "gp",
                    query: {
                        query_string: {
                            default_field: "gp.gp",
                            query: _query,
                        },
                    },
                },
                nested: {
                    path: "nd",
                    query: {
                        query_string: {
                            default_field: "nd.ndId",
                            query: _query,
                        },
                    },
                },
                nested: {
                    path: "gn",
                    query: {
                        query_string: {
                            default_field: "gn.gnId",
                            query: _query,
                        },
                    },
                },
            },
        };
        const body5 = {
            query: {
                bool: {
                    should: [
                        {
                            nested: {
                                path: "md",
                                query: {
                                    bool: {
                                        must: [
                                            {
                                                terms: {
                                                    "md.mdId": _query,
                                                },
                                            },
                                        ],
                                    },
                                },
                                boost: _scores.models,
                            },
                        },
                        {
                            nested: {
                                path: "mk",
                                query: {
                                    bool: {
                                        must: [
                                            {
                                                terms: {
                                                    "mk.mkId": _query,
                                                },
                                            },
                                        ],
                                    },
                                },
                                boost: _scores.makes,
                            },
                        },
                        {
                            nested: {
                                path: "mf",
                                query: {
                                    bool: {
                                        must: [
                                            {
                                                terms: {
                                                    "mf.mfId": _query,
                                                },
                                            },
                                        ],
                                    },
                                },
                                boost: _scores.modifs,
                            },
                        },
                        {
                            nested: {
                                path: "gp",
                                query: {
                                    bool: {
                                        must: [
                                            {
                                                terms: {
                                                    "gp.gp": _query,
                                                },
                                            },
                                        ],
                                    },
                                },
                                boost: _scores.genparts,
                            },
                        },
                        {
                            nested: {
                                path: "gn",
                                query: {
                                    bool: {
                                        must: [
                                            {
                                                terms: {
                                                    "gn.gnId": _query,
                                                },
                                            },
                                        ],
                                    },
                                },
                                boost: _scores.partnodegroups,
                            },
                        },
                        {
                            nested: {
                                path: "nd",
                                query: {
                                    bool: {
                                        must: [
                                            {
                                                terms: {
                                                    "nd.ndId": _query,
                                                },
                                            },
                                        ],
                                    },
                                },
                                boost: _scores.partnodes,
                            },
                        },
                    ],
                },
            },
        };
        const partans = await client.search({
            index: "partans",
            body: body5,
        });
        console.log("partans.hits.hits", partans.hits.hits.length);
        for (let i in partans.hits.hits) {
            console.log(
                "partans.hits.hits[i]._source",
                partans.hits.hits[i]._source
            );
            console.log(
                "partans.hits.hits[i]._score",
                partans.hits.hits[i]._score
            );
        }
        console.log("========================");
    } catch (error) {
        console.trace(error.message);
    }
};

(async function _start() {
    const q = "aveo передний бампер шасси chevrolet 2011";
    // const q = "КОМПЛЕКТ ПЕРЕДНИХ КОЛОДОК chevrolet aveo 2011";
    const makes = searchMakes(q);
    const models = searchModels(q);
    const modifs = searchModifs(q);
    const partNodeGroups = searchPartNodeGroups(q);
    const partNodes = searchPartNodes(q);
    const genParts = searchGenParts(q);

    console.log("await makes", await makes);

    const consolidated = (await makes)
        .concat(await models)
        .concat(await modifs)
        .concat(await partNodeGroups)
        .concat(await partNodes)
        .concat(await genParts);
    // const consolidated = await makes;

    console.log(consolidated);
    let string = "";
    const ids = [];
    const scores = {
        makes: 1,
        models: 1,
        modifs: 1,
        partnodegroups: 1,
        partnodes: 1,
        genparts: 1,
    };
    const sq = {
        makes: [],
        models: [],
        modifs: [],
        partnodegroups: [],
        partnodes: [],
        genparts: [],
    };
    for (let i in consolidated) {
        string += consolidated[i]._id + " ";
        scores[consolidated[i]._index] += consolidated[i]._score;
        ids.push(mongoose.Types.ObjectId(consolidated[i]._id));
        // scores[consolidated[i]._id] = consolidated[i]._score;
    }
    // console.log(string);
    // console.log(ids);
    console.log(scores);

    const partans = searchPartan(ids, scores);
    //     const partans = await axios({
    //         method: "post",
    //         url: `http://localhost:3031/partan/search`,
    //         data: {
    //             cond: { "mk.mkId": { $in: ids } },
    //             scores: scores,
    //         },
    //         headers: {
    //             "Content-Type": "application/json",
    //         },
    //     });
    //     console.log(partans.data);
})();
