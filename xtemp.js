const fs = require("fs");

const x = [
    {
        img: "T06/6/2T6720GW.jpg",
        mf: [{ id: "5f8e736f62b1144467547ac5", score: 100 }],
        nd: [{ id: "5f8e7def62b1144467547aca", score: 100 }],
    },
    {
        img: "T06/6/2T6723E.jpg",
        mf: [{ id: "5f8e736f62b1144467547ac5", score: 100 }],
        nd: [{ id: "5f8e7def62b1144467547aca", score: 100 }],
    },
    {
        img: "T06/6/2T6730DGW.jpg",
        mf: [{ id: "5f8e736f62b1144467547ac5", score: 100 }],
        nd: [{ id: "5f8e7e0a62b1144467547acb", score: 100 }],
    },
    {
        img: "T06/6/2T6732DGW.jpg",
        mf: [{ id: "5f8e736f62b1144467547ac5", score: 100 }],
        nd: [{ id: "5f8e7e0a62b1144467547acb", score: 100 }],
    },
    {
        img: "T06/4/2T4510.jpg",
        mf: [{ id: "5f8e736f62b1144467547ac5", score: 100 }],
        nd: [{ id: "5f8e7e2262b1144467547acc", score: 100 }],
    },

    {
        img: "T02/6/RT6720GW.jpg",
        mf: [{ id: "5f8e739c62b1144467547ac6", score: 100 }],
        nd: [{ id: "5f8e7def62b1144467547aca", score: 100 }],
    },
    {
        img: "T02/4/RT4510.jpg",
        mf: [{ id: "5f8e739c62b1144467547ac6", score: 100 }],
        nd: [{ id: "5f8e7e2262b1144467547acc", score: 100 }],
    },
    {
        img: "T02/6/RT6730DG.jpg",
        mf: [{ id: "5f8e7e0a62b1144467547acb", score: 100 }],
        nd: [{ id: "5f8e7def62b1144467547aca", score: 100 }],
    },
    {
        img: "V06/6/V26720G.jpg",
        mf: [{ id: "5f8e73cb62b1144467547ac7", score: 100 }],
        nd: [{ id: "5f8e7def62b1144467547aca", score: 100 }],
    },
    {
        img: "V06/4/V24510.jpg",
        mf: [{ id: "5f8e73cb62b1144467547ac7", score: 100 }],
        nd: [{ id: "5f8e7e2262b1144467547acc", score: 100 }],
    },
    {
        img: "V06/6/V26730GW.jpg",
        mf: [{ id: "5f8e73cb62b1144467547ac7", score: 100 }],
        nd: [{ id: "5f8e7e0a62b1144467547acb", score: 100 }],
    },
];

fs.writeFileSync("xtemp.json", JSON.stringify(x));
// console.log("completed", js1);
// console.log(x);
