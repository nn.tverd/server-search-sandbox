var elasticsearch = require("elasticsearch");
var client = new elasticsearch.Client({
    host: "localhost:9200",
    log: "trace",
    apiVersion: "7.2", // use the same version of your Elasticsearch instance
});

client.ping(
    {
        // ping usually has a 3000ms timeout
        requestTimeout: 5000,
    },
    function (error) {
        if (error) {
            console.trace("elasticsearch cluster is down!");
        } else {
            console.log("All is well");
        }
    }
);

async function putSettings() {
    const newSettings = {
        // index: "v2genparts",
        // index: "v2partnodes",
        // index: "v2partnodegroups",
        // type: "_doc",
        payload: {
            settings: {
                analysis: {
                    filter: {
                        russian_stop: {
                            type: "stop",
                            stopwords: "_russian_",
                        },
                        russian_keywords: {
                            type: "keyword_marker",
                            keywords: ["пример"],
                        },
                        russian_stemmer: {
                            type: "stemmer",
                            language: "russian",
                        },
                    },
                    analyzer: {
                        rebuilt_russian: {
                            tokenizer: "standard",
                            filter: [
                                "lowercase",
                                "russian_stop",
                                "russian_keywords",
                                "russian_stemmer",
                            ],
                        },
                    },
                },
            },
        },
    };

    try {
        const response = await client.indices.create({
            // const response = await client.indices.putSettings({
            index: newSettings.index,
            // type: '_doc',
            body: newSettings.payload,
        });
        console.log(response);
    } catch (error) {
        console.trace(error.message);
    }
}

(async function _start() {
    // try {
    //     const response = await client.indices.getSettings({
    //         index: "genparts",
    //     });
    //     console.log(response);
    // } catch (error) {
    //     console.trace(error.message);
    // }

    // await client.indices.create({
    //     index: "partans",
    //     body: {
    //         settings: {
    //             index: {
    //                 number_of_replicas: 0, // for local development
    //             },
    //         },
    //     },
    // });

    await putSettings();
})();
