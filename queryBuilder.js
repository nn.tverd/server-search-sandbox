const fs = require("fs");

const js1 = {
    mappings: {
        doc: {
            properties: {
                myfield1: {
                    type: "text",
                    fields: {
                        keyword: {
                            type: "keyword",
                            ignore_above: 256,
                        },
                    },
                    analyzer: "rebuilt_russian",
                },
            },
        },
    },
};

fs.writeFileSync("kibana.json", JSON.stringify(js1));
console.log("completed", js1);
